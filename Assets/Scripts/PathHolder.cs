﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PathHolder : MonoBehaviour {

    [SerializeField]
    public Transform PointsArray;
    public Transform[] Path;

    public void SetPathPoints()
    {
        if (PointsArray.childCount < 0)
        {
            return;
        }

        Path = new Transform[PointsArray.childCount];
        for (int i = 0; i<PointsArray.childCount; i++)
        {
            Path[i] = PointsArray.GetChild(i);
        }
    }
}
