﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FBAnalytics : InGameObject
{

    void Awake()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            //Handle FB.Init
            FB.Init(() =>
            {
                FB.ActivateApp();
            });
        }
		base.Awake();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        // Check the pauseStatus to see if we are in the foreground
        // or background
        if (!pauseStatus)
        {
            //app resume
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                //Handle FB.Init
                FB.Init(() =>
                {
                    FB.ActivateApp();
                });
            }
        }
    }

    protected override void gameLoad()
    {
        FB.LogAppEvent(
            "gameLoad"
            );
    }

    protected override void gameStart()
    {
        FB.LogAppEvent(
            "gameStart"
            );
    }

    protected override void gamePause()
    {
        FB.LogAppEvent(
            "gamePause"
            );
    }
    protected override void gameUnpause()
    {
        FB.LogAppEvent(
            "gameUnpause"
            );
    }
    protected override void gameOver()
    {
        FB.LogAppEvent(
            "gameOver"
            );
    }
}
