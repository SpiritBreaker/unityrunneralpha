﻿using UnityEngine;
using System.Collections;

//http://www.habrador.com/tutorials/interpolation/1-catmull-rom-splines/




public class MoveOnPathScript2 : InGameObject
{
    public ISpline Spline;

    public Camera camera;
    public GameObject aim_object;
    public GameObject pathWalker;
    public float up_vector = 1.0f;

    public IControl Control;

    public float angle = 0;

    Vector3 p0, p1, p2, p3;
    Vector3 c0, c1, c2, c3;

    Vector3 tangent, normal, binormal;
 
    float t = 0.0f;
    int previous_t = -1;

    private float rotation = 0.0f;

    private SplinePoint currentPos;
    private SplinePoint lastPos;


    public float acceleration;


    new void Start()
    {
        base.Start();
        t = Player.Instance.position;
        this.angle = Player.Instance.angle;
        Spline =  CompositionRoot.Instance.spline;

        currentPos = Spline.GetPoint((int)t, t %1 );
        lastPos = currentPos;

        //Control = gameObject.AddComponent<LegacyControlSwipe>();

    }

    void get_p_c()
    {
        if ((int)t > Spline.AmountOfPoints - 1)
        {
            t = 0.0f;
        }

        p0 = Spline.GetKnot(  (int)MyMath.mod((int)t - 1, Spline.AmountOfPoints)  ).position;
        p1 = Spline.GetKnot(  (int)MyMath.mod((int)t,     Spline.AmountOfPoints)  ).position;
        p2 = Spline.GetKnot(  (int)MyMath.mod((int)t + 1, Spline.AmountOfPoints)  ).position;
        p3 = Spline.GetKnot(  (int)MyMath.mod((int)t + 2, Spline.AmountOfPoints)  ).position;

        c0 = Spline.GetKnot(  (int)MyMath.mod((int)t - 2, Spline.AmountOfPoints)  ).position;
        c1 = Spline.GetKnot(  (int)MyMath.mod((int)t - 1, Spline.AmountOfPoints)  ).position;
        c2 = Spline.GetKnot(  (int)MyMath.mod((int)t,     Spline.AmountOfPoints)  ).position;
        c3 = Spline.GetKnot(  (int)MyMath.mod((int)t + 1, Spline.AmountOfPoints)  ).position;

    }

    // Update is called once per frame
    void Update()
    {
        t += Player.Instance.speed * Time.deltaTime;

        if ((int)t != previous_t)
        {
            previous_t = (int)t;
            get_p_c();
        }

        Player.Instance.position = (int)t;
        Player.Instance.segment_position = t % 1;
        currentPos = Spline.GetPoint((int)t, t %1 );
        Player.Instance.traveledDistance += Vector3.Distance(currentPos.position, lastPos.position);
        lastPos = currentPos;

        tangent = Spline.GetTangent(t % 1, p0, p1, p2, p3).normalized;
        normal = Vector3.Cross(tangent, aim_object.transform.up).normalized;
        binormal = Vector3.Cross(normal, tangent).normalized;
        tangent = Vector3.Cross(binormal, normal).normalized;

        //aim_object.transform.rotation = Quaternion.LookRotation(tangent);
        aim_object.transform.Rotate(tangent, aim_object.GetComponent<Rotate>().currentAngle, Space.World);


        aim_object.transform.localPosition = Spline.GetWorldPosition(t % 1, p0, p1, p2, p3) + (aim_object.transform.up * Player.Instance.height);
     

        camera.transform.position = Spline.GetWorldPosition(t % 1, c0, c1, c2, c3) + (camera.transform.up * Player.Instance.height);
        camera.transform.LookAt(aim_object.transform.position, aim_object.transform.up);
    }
}