﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PathHolder))]
[ExecuteInEditMode]
public class Spline : MonoBehaviour
{

    public ISpline spline;

    public PathHolder pathHolder;
    bool isLooping = true;
    public bool draw = true;

    // Use this for initialization
    void Awake()
    {
        pathHolder = this.GetComponent<PathHolder>();
        spline = new CatmullRomSpline(pathHolder);
    }

#if (UNITY_EDITOR)
    // Update is called once per frame
    void Update()
    {
		if (spline == null)
		{
			spline = new CatmullRomSpline(pathHolder);
		}
        if(pathHolder == null)
        {
            pathHolder = this.GetComponent<PathHolder>();
        }
    }

    //Display without having to press play
    void OnDrawGizmos()
    {
        //Draw the Catmull-Rom spline between the points
        for (int i = 0; i < pathHolder.Path.Length; i++)
        {
            //Cant draw between the endpoints
            //Neither do we need to draw from the second to the last endpoint
            //...if we are not making a looping line
            if ((i == 0 || i == pathHolder.Path.Length - 2 || i == pathHolder.Path.Length - 1) && !isLooping)
            {
                continue;
            }

            if (draw)
            {
                DisplayCatmullRomSpline(i);
            }
        }
    }

    //Display a spline between 2 points derived with the Catmull-Rom spline algorithm
    void DisplayCatmullRomSpline(int pos)
    {
        //The 4 points we need to form a spline between p1 and p2
        Vector3 p0 = pathHolder.Path[ClampListPos(pos - 1)].position;
        Vector3 p1 = pathHolder.Path[pos].position;
        Vector3 p2 = pathHolder.Path[ClampListPos(pos + 1)].position;
        Vector3 p3 = pathHolder.Path[ClampListPos(pos + 2)].position;

        //The start position of the line
        Vector3 lastPos = p1;

        //The spline's resolution
        //Make sure it's is adding up to 1, so 0.3 will give a gap, but 0.2 will work
        float resolution = 0.2f;

        //How many times should we loop?
        int loops = Mathf.FloorToInt(1f / resolution);

        for (int i = 1; i <= loops; i++)
        {
            //Which t position are we at?
            float t = i * resolution;

            //Find the coordinate between the end points with a Catmull-Rom spline
            Vector3 newPos = spline.GetWorldPosition(t, p0, p1, p2, p3);

            //Draw this line segment
            Gizmos.color = new Color(i / 10.0f, i / 10.0f, i / 10.0f);
            Gizmos.DrawLine(lastPos, newPos);

            //Save this pos so we can draw the next line segment
            lastPos = newPos;
        }
    }

    //Clamp the list positions to allow looping
    int ClampListPos(int pos)
    {
        if (pos < 0)
        {
            pos = pathHolder.Path.Length - 1;
        }

        if (pos > pathHolder.Path.Length)
        {
            pos = 1;
        }
        else if (pos > pathHolder.Path.Length - 1)
        {
            pos = 0;
        }

        return pos;
    }
#endif
}
