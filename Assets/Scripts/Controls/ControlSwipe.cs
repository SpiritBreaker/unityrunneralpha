﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;



public class ControlSwipe : IControl {

	Vector3 mousePressedPosition;
	Vector3 mousePosition;
	Vector3 mouseReleasedPosition;
    Vector3 bottomCenter = new Vector3(0.5f, 0.0f, 0.0f);

    Vector3 startVector;
    Vector3 endVector;

    float angleBetween;
    float previousAngle;

	Vector3 mousePreviousPosition;
	float beginAngle;

    float velocity;
    Coroutine velocityCoroutine;
    Coroutine easyInOutCoroutine;

    float countDown = 0.2f;
    float countDownTimer = 0.0f;

	private float beginTime;

    float speed;

	// Use this for initialization
	void Start () 
    {

	}

	void MousePressed()
	{
		acceleration.x = 0f;
		beginAngle = Player.Instance.angle;
		mousePressedPosition = Input.mousePosition;
		beginTime = Time.time;
        startVector =  Camera.main.ScreenToViewportPoint(Input.mousePosition) - bottomCenter;
	}

	void MouseMove()
	{
        //which distance finger is travel in a unit of time;
        endVector =  Camera.main.ScreenToViewportPoint(Input.mousePosition) - bottomCenter;
        int sign = Vector3.Cross(startVector, endVector).z < 0 ? -1 : 1;        
		angleBetween = sign  * Vector3.Angle(startVector, endVector);

        acceleration.output = angleBetween - previousAngle; 
		acceleration.angle = angleBetween + beginAngle; 


        previousAngle = angleBetween; 

	}

    IEnumerator easyInOut()
    {
        float x = 0.0f;
        while (velocity > 0.001f)
        {
            velocity = 10.0f*(Mathf.Pow(0.7f, x));
            x+=1f * Time.deltaTime;
            yield return null;
        }
        yield break;

    }


    IEnumerator velocityAngle()
    {
        if (acceleration.x > 0)
        {
            while (acceleration.x > 0.0f)
            {
                acceleration.angle += 30f * acceleration.x * Time.deltaTime;
                acceleration.x -= (10f - velocity) * Time.deltaTime;
                yield return null;
            }
            yield break;
        }
        else{
            while (acceleration.x < 0.0f)
            {
                acceleration.angle += 30f * acceleration.x * Time.deltaTime;
                acceleration.x += (10f - velocity) * Time.deltaTime;
                yield return null;
            }
            yield break;
            }
    }

	void MouseReleased()
	{
        acceleration.x = acceleration.output;//(angleBetween/20f)/(Time.time - beginTime);
        velocity = 10f;

        if (velocityCoroutine !=null )
        {
            StopCoroutine(velocityCoroutine);
            velocityCoroutine = StartCoroutine(velocityAngle());

            StopCoroutine(easyInOutCoroutine);
            easyInOutCoroutine = StartCoroutine(easyInOut());
        }
        else
        {
            velocityCoroutine = StartCoroutine(velocityAngle());
            easyInOutCoroutine = StartCoroutine(easyInOut());
        }
	}

	// Update is called once per frame
	void Update () {

        //which distance finger is travel in a unit of time;
		if (!EventSystem.current.currentSelectedGameObject)
		{
			if (Input.GetMouseButtonDown(0))
			{
				MousePressed();
			}

			if (Input.GetMouseButton(0))
			{
                MouseMove();   		
			}	

			if (Input.GetMouseButtonUp(0))
			{

				MouseReleased();
			}
		}


	}
}
