﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlGyro : IControl{

	void Start()
	{
		Input.gyro.enabled = true;
	}

	void Update()
	{
		acceleration.x = Input.acceleration.x;
	}
}
