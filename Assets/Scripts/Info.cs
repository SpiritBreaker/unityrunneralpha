﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Info : MonoBehaviour {
	
	private Player player;
	private ISpline spline;

	[SerializeField]
	public int playerPosition;
	[SerializeField]
	public float playerSegmentPosition;

	[SerializeField]
	public int point_to_playerPosition;
	[SerializeField]
	public float point_to_playerSegmentPosition;



	public double splineLength;

	[SerializeField]
	public double relative_position;
	
	[SerializeField]
	public double absolute_position;

	// Use this for initialization
	void Start () {
			spline = CompositionRoot.Instance.spline;
			player = Player.Instance;
			splineLength = spline.Length();
	}
	
	// Update is called once per frame
	void Update () {
		
		playerPosition= player.position;
		playerSegmentPosition = player.segment_position;

		relative_position = spline.PointToDistance(playerPosition, playerSegmentPosition, Space.World);
		spline.DistanceToPoint(relative_position);
	}
}
