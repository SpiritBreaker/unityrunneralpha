﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;
using System.IO;

[Serializable]
public class Account : InGameObject
{

    #region Singleton
    private static Account _instance;
    public static Account Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Account>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("Account");
                    _instance = container.AddComponent<Account>();
                }
            }
            return _instance;
        }
    }
    #endregion

    public int lives = 3;
    public List<bool> levels = new List<bool>();
    public int totalCoins = 0;
    public int totalGems = 0;
    public int bestScores = 0;
    public int timeFreezes = 2;
    public int shields = 2;
    public int noAd = 0;
    public int volume = 1;
    public int music = 1;
    public int controlsType = 0;

    string filePath = "/account.json";


    string stPath(string path)
    {

        return Application.persistentDataPath + filePath;
    }

    void CheckFirstLaunch()
    {
        SaveAccount();
    }

    void SaveAccount()
    {
        string json = JsonUtility.ToJson(this, true);
        StreamWriter writer = new StreamWriter(stPath(filePath));
        writer.WriteLine(json);
        writer.Close();
    }

    void LoadAccount()
    {
        if (File.Exists(stPath(filePath)))
        {
            string dataAsJson = File.ReadAllText(stPath(filePath));
            JsonUtility.FromJsonOverwrite(dataAsJson, this);
        }
        else
        {
            CheckFirstLaunch();
            LoadAccount();
        }
    }

    protected override void gameOver()
    {
        if (Player.Instance.traveledDistance > bestScores)
        {
            bestScores = (int)Player.Instance.traveledDistance;
        }

        totalCoins += Player.Instance.coins;
        SaveAccount();
        base.gameOver();
    }

    protected override void gameLoad()
    {
        LoadAccount();
        base.gameLoad();
    }
    void OnApplicationQuit()
    {
        gameOver();
    }
}
