﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perk : InteractiveObject {


    protected void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("player"))
        {
            Destroy(this.gameObject);
        }
        if(other.gameObject.CompareTag("culling_plane"))
        {
            poolManager.Despawn(this.gameObject);
        }
    }

}
