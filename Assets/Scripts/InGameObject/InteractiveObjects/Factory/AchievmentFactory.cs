﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievmentFactory : InGameObject, IFactory
{

    PoolManager poolManager;

    public double position;

    public List<GameObject> productionLines = new List<GameObject>();
    IProductionLine activeProductionLine;

    public bool isPaused = false;
    public IEnumerator coroutine;
    public ISpline spline;

    IFactory factory;

    public IEnumerator InstantiateOverTime()
    {
        //Bug is here//
        double pos = spline.PointToDistance(Player.Instance.position, Player.Instance.segment_position);
        //position = activeProductionLine.Create(poolManager, spline, position, TileMap map);
        yield return null;
    }


new void Start()
{
    base.Start();
    if (poolManager == null)
    {
        poolManager = gameObject.AddComponent<PoolManager>();
    }
    spline = CompositionRoot.Instance.spline;
    activeProductionLine = productionLines[0].GetComponent<IProductionLine>() as IProductionLine;
}


void Update()
{
}

protected override void gameStart()
{
    isPaused = false;
    coroutine = InstantiateOverTime();
    StartCoroutine(coroutine);
    base.gameStart();
}

protected override void gamePause()
{
    isPaused = true;
    base.gamePause();
}

protected override void gameUnpause()
{
    isPaused = false;
    base.gameUnpause();
}

protected override void gameOver()
{
    isPaused = true;
    StopCoroutine(coroutine);
    base.gameOver();
}

}
