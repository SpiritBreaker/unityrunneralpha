﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IProductionQueue {

	public abstract void Push(IProductionLine productionLine);
	public abstract IProductionLine Pop();

}
