﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IProductionLine : MonoBehaviour   {

	public abstract double Create(PoolManager poolManager, ISpline spline, double begin);
}

