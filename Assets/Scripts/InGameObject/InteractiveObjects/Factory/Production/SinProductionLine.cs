﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinProductionLine : IProductionLine
{

    [SerializeField]
    public GameObject prefab;

    [SerializeField]
    public float destroyOnFinish;

    [SerializeField]
    public double step;

    [SerializeField]
    public double length;

    private int[] sign = {-1, 1};

    private PoolManager poolManager;

    List<GameObject> products = new List<GameObject>();


    public GameObject CreateSingleObject()
    {
        GameObject instance = poolManager.Spawn(prefab);
        InteractiveObject settings = instance.GetComponent<InteractiveObject>();
        settings.poolManager = poolManager;
        settings.destroyOnFinish = true;

        return instance;
    }

    public override double Create(PoolManager poolManager, ISpline spline, double begin)
    {
        this.poolManager = poolManager;
        double point = begin;
        int number = (int)(length / step);

        float rotation = Random.Range(0, 360);
        float RandomSign = sign[Random.Range(0, 2)];

        for (int i = 0; i < number; i++)
        {
            rotation += RandomSign * 15f;
            GameObject newInstance = CreateSingleObject();
            InteractiveObject settings = newInstance.GetComponent<InteractiveObject>();
            settings.point = spline.DistanceToPoint(point);
            settings.angle = rotation;

            newInstance.transform.forward = settings.point.tangent;
            newInstance.transform.position = settings.point.position + (newInstance.transform.up * settings.height);
            newInstance.transform.RotateAround(settings.point.position, settings.point.tangent, settings.angle);

            point += step;
        }

        return point;
    }

}
