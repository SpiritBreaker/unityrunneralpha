﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMapProductionLine : IProductionLine
{
    private PoolManager poolManager;

    public List<GameObject> tileMaps = new List<GameObject>();

    private List<SplineGameTile> tiles;

    public GameObject CreateSingleObject(GameObject prefab)
    {
        GameObject instance = poolManager.Spawn(prefab);
        InteractiveObject settings = instance.GetComponent<InteractiveObject>();
        settings.poolManager = poolManager;
        settings.destroyOnFinish = true;
        return instance;
    }

    void LoadTiles()
    {
        tiles = new List<SplineGameTile>();
        for (int i = 0; i < tileMaps.Count; i++)
        {
            SplineGameTile gameTile = TileMapAdapter.AdaptTileMap(tileMaps[i].GetComponent<TileMap>());

            tiles.Add(gameTile);
        }
    }

    public override double Create(PoolManager poolManager, ISpline spline, double begin)
    {
        if (tiles == null)
        {
            LoadTiles();
        }

        int randomAngle = Random.Range(0, 360);

        this.poolManager = poolManager;
        int n = Random.Range(0, tiles.Count);

        for (int i = 0; i < tiles[n].inGameObject.Count; i++)
        {
            GameObject newInstance = CreateSingleObject(tiles[n].prefabs[i]);
            InteractiveObject settings = newInstance.GetComponent<InteractiveObject>();

            settings.point = spline.DistanceToPoint(begin + tiles[n].localPosition[i]);
            settings.angle = randomAngle + tiles[n].angle[i];
            settings.distance = begin + tiles[n].localPosition[i];

            newInstance.transform.forward = settings.point.tangent;
            newInstance.transform.rotation =   Quaternion.LookRotation(settings.point.tangent, settings.point.binormal) * tiles[n].localRotation[i];
            newInstance.transform.position = settings.point.position + (newInstance.transform.up * settings.height + 
            new Vector3( 0f, tiles[n].localVec3Pos[i].y, 0f));
            newInstance.transform.localScale = tiles[n].localScale[i];
            newInstance.transform.RotateAround(settings.point.position, settings.point.tangent, settings.angle);
        }

        // Randomly take coins
        for (int i = 0; i < tiles[n].coinsSpline.Count; i++)
        {
            GameObject newInstance = CreateSingleObject(tiles[n].coinsPrefab.gameObject);
            InteractiveObject settings = newInstance.GetComponent<InteractiveObject>();

            settings.point = spline.DistanceToPoint(begin + tiles[n].coinsLocalPosition[i]);
            settings.angle = randomAngle + tiles[n].coinsAngle[i];

            newInstance.transform.forward = settings.point.tangent;
            newInstance.transform.position = settings.point.position + (newInstance.transform.up * settings.height);
            newInstance.transform.RotateAround(settings.point.position, settings.point.tangent, settings.angle);
        }

        return begin + tiles[n].length;
    }

}
