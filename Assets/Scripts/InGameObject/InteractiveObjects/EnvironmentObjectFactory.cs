﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentObjectFactory : InGameObject, IFactory
{


    [SerializeField]
    public float nearPlane = 40.0f;
    [SerializeField]
    public double startPos = 50;

    private PoolManager poolManager;

    public List<GameObject> productionLines = new List<GameObject>();
    private IProductionLine activeProductionLine;

    public bool isPaused = false;
    public IEnumerator coroutine;
    public ISpline spline;

    public double endPos;
    public double currentPos;

    public IEnumerator InstantiateOverTime()
    {
        //Bug is here//
        double pos = spline.PointToDistance(Player.Instance.position, Player.Instance.segment_position);
        endPos = activeProductionLine.Create(poolManager, spline, pos + startPos);
        yield return null;

        while (true)
        {
            //Get current position of the spline
            currentPos = spline.PointToDistance(Player.Instance.position, Player.Instance.segment_position);

            // get the delta value between current position and endposition of the current tilemap chunk
            double delta = (endPos - currentPos);

            // get sign of delta
            float sign = Mathf.Sign((float)delta);

            // split delta on two cases
            // First if sign is negative - which is means current position somewhere
            // close to the end of the spline and end position somewhere over the begining
            // point of the spline

            if (sign  < 0)
            {
                delta = (spline.Length() - currentPos) + endPos;
            }


            if (delta < nearPlane)
            {
                endPos = activeProductionLine.Create(poolManager, spline, endPos);
                if (endPos > spline.Length())
                {
                    endPos = endPos - spline.Length();
                }
            }
            yield return null;
        }
    }


    new void Start()
    {
        base.Start();
        if (poolManager == null)
        {
            poolManager = gameObject.AddComponent<PoolManager>();
        }
        spline = CompositionRoot.Instance.spline;
        activeProductionLine = productionLines[0].GetComponent<IProductionLine>() as IProductionLine;
    }


    public void Update()
    {
        activeProductionLine = productionLines[gameObject.GetComponent<Randomness>().random_value].GetComponent<IProductionLine>() as IProductionLine;
    }

    protected override void gameLoad()
    {
        isPaused = false;
        coroutine = InstantiateOverTime();
        StartCoroutine(coroutine);
        base.gameLoad();
    }

    protected override void gamePause()
    {
        isPaused = true;
        base.gamePause();
    }

    protected override void gameUnpause()
    {
        isPaused = false;
        base.gameUnpause();
    }

    // protected override void gameOver()
    // {
    //     isPaused = true;
    //     StopCoroutine(coroutine);
    //     base.gameOver();
    // }

}