﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ISplineDriven : InteractiveObject {

    public ISpline Spline;
    public float startPosition;
    [SerializeField]
	public float speed;
	public int direction;

    public int position;
    public float segment_position;

    float t = 0.0f;
    int previous_t = -1;

    Vector3 p0, p1, p2, p3;

    Vector3 tangent, normal, binormal;

    public new void Start()
	{
        base.Start();
        //t = Player.Instance.position + 1;
        t = startPosition;
		Spline = CompositionRoot.Instance.spline;
	}

    void get_p_c()
    {
        if ((int)t > Spline.AmountOfPoints - 1)
        {
            t = 0.0f;
        }

        p0 = Spline.GetKnot(  (int)MyMath.mod((int)t - 1, Spline.AmountOfPoints)  ).position;
        p1 = Spline.GetKnot(  (int)MyMath.mod((int)t,     Spline.AmountOfPoints)  ).position;
        p2 = Spline.GetKnot(  (int)MyMath.mod((int)t + 1, Spline.AmountOfPoints)  ).position;
        p3 = Spline.GetKnot(  (int)MyMath.mod((int)t + 2, Spline.AmountOfPoints)  ).position;
    }

    new void FixedUpdate()
    {
        t += 2f * Time.deltaTime;

        if ((int)t != previous_t)
        {
            previous_t = (int)t;
            get_p_c();
        }

        this.position = (int)t;
        this.segment_position = t % 1;

        this.point = Spline.GetPoint((int)t, t % 1);
        tangent = Spline.GetTangent(t % 1, p0, p1, p2, p3).normalized;
        normal = Vector3.Cross(tangent, this.transform.up).normalized;
        binormal = Vector3.Cross(normal, tangent).normalized;
        tangent = Vector3.Cross(binormal, normal).normalized;

        this.transform.forward = tangent;
        this.transform.position = Spline.GetWorldPosition(t % 1, p0, p1, p2, p3) + (this.transform.up * this.height);

        angle -= Time.deltaTime * angle_speed * angle_animation;
        angle = angle % 360;
        this.transform.RotateAround(this.point.position, this.transform.forward, angle);
    }

}
