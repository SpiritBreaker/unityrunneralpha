﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCoin : Reward {

    new void Start()
    {
        base.Start();
    }

    protected new void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("player"))
        {
           Player.Instance.coins +=1;
           base.emmitSound(onEnterSound, 1f);
        }

        base.OnCollisionEnter(other);
    }
}
