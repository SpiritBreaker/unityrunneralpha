﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


public class OldAccount : InGameObject {

    #region Singleton
    private static Account _instance;
    public static Account Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Account>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("Account");
                    _instance = container.AddComponent<Account>();
                }
            }
            return _instance;
        }
    }
    #endregion

    #region Variables
    
    //gameplay
    [SerializeField]
    private int m_lives = 3;
    public int lives { get { return m_lives; } set { m_lives = value; } }

    // [SerializeField]
    // private List<bool> m_levels = new List<bool>();
    // public List<bool> levels { get { return m_levels; } set { m_levels = value; } }

    [SerializeField]
    private int m_totalCoins = 0;
    public int totalCoins { get { return m_totalCoins; } set { m_totalCoins = value; } }

    [SerializeField]
    private int m_bestScores = 0;
    public int bestScores { get { return m_bestScores; } set { m_bestScores = value; } }

    [SerializeField]
    private int m_timeFreezes = 2;
    public int timeFreezes { get { return m_timeFreezes; } set { m_timeFreezes = value; } }

    [SerializeField]
    private int m_shields = 2;
    public int shields { get { return m_shields; } set { m_shields = value; } }


    [SerializeField]
    private int m_noAd = 0;
    public int noAd { get { return m_noAd; } set { m_noAd = value; } }

    //settings
    [SerializeField]
    private int m_volume = 1;
    public int volume { get { return m_volume; } set { m_volume = value; } }

    [SerializeField]
    private int m_music = 1;
    public int music { get { return m_music; } set { m_music = value; } }

    [SerializeField]
    private int m_controlsType = 0;
    public int controlsType { get { return m_controlsType; } set { m_controlsType = value; } }

    [SerializeField]

    #endregion

    protected override void gameOver()
    {
        if (Player.Instance.traveledDistance > m_bestScores)
        {
            m_bestScores = (int)Player.Instance.traveledDistance;
        }

        m_totalCoins += Player.Instance.coins;

        base.gameOver();
    }

    void OnApplicationQuit()
    {        
        gameOver();
    }
}
