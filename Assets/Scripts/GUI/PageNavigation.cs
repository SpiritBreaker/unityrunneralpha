﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Interface
interface INavigationCommand
{
    void Do();
    void Undo();
}

#endregion

public enum NavigationAction { Next, Back };

public class NavigationCommand : INavigationCommand
{

    NavigationAction action;
    bool keepVisible;
    PageNavigation pageNavigation;
    UIPage currentPage;
    UIPage nextPage;

    public NavigationCommand(ref PageNavigation pageNavigation, UIPage currentPage, UIPage nextPage, NavigationAction action, bool keepVisible)
    {
        this.pageNavigation = pageNavigation;
        this.action = action;
        this.keepVisible = keepVisible;
        this.currentPage = currentPage;
        this.nextPage = nextPage;
    }

    public void Do()
    {
        pageNavigation.FlipThePage(this.currentPage, this.nextPage, this.keepVisible);
    }

    public void Undo()
    {
        pageNavigation.FlipThePage(this.nextPage, this.currentPage, this.keepVisible);
    }
}


public class NavigationCommandList : List<NavigationCommand>, INavigationCommand
{
    public void Do()
    {
        this[this.Count - 1].Do();
    }

    public void Undo()
    {
        this[this.Count - 1].Undo();
        this.RemoveAt(this.Count - 1);

    }
}
public class PageNavigation : MonoBehaviour
{

    public UIPage currentPage;
    public UIPage nextPage;
    public UIPage previousPage;

    public NavigationCommandList navigationCommandsList = new NavigationCommandList();

    public IEnumerator LateCall(UIPage current, UIPage next, bool keepVisible)
    {
        //yield return new WaitForEndOfFrame();
        next.gameObject.SetActive(true);
        if (keepVisible)
        {

        }
        else
        {
            current.gameObject.SetActive(false);
        }

        this.currentPage = next;
        this.previousPage = current;
        yield break;
    }

    public void FlipThePage(UIPage current, UIPage next, bool keepVisible)
    {
        StartCoroutine(LateCall(current, next, keepVisible));
    }
}

