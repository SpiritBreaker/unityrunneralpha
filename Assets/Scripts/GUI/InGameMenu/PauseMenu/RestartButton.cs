﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartButton : MonoBehaviour {

	public void OnPointerDown()
	{
		Game.Instance.gameUnpause();
		Game.Instance.gameRestart();
	}
}
