﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuButton : MonoBehaviour
{

    public void OnPointerDown()
    {
        UIDialogBox dialogBox = Instantiate(UIManager.Instance.YesNoDialog);
        dialogBox.PopUpDialog("Title", "Do you want to quick\nall your progress will be lost", OK, Cancel);

		//dialogBox.transform.SetParent(topmost.gameObject.transform);
    }


    void OK()
    {
        this.GetComponent<UIFlipThePage>().OnPointerDown();
        Game.Instance.gameUnpause();
        Game.Instance.gameOver();
    }

    void Cancel()
    {

    }
}
