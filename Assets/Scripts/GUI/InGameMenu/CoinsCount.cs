﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsCount : MonoBehaviour {

	private Text text;

	void Start()
	{
		text = this.gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		text.text = ((int)Player.Instance.coins).ToString();		
	}
}
