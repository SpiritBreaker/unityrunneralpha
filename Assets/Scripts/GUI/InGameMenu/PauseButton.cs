﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButton : MonoBehaviour {

    public virtual void OnPointerDown()
    {
		  Game.Instance.gamePause();
    }
}
