﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoAd : UIObject {

	protected override void becomeVisible()
	{
		if(Account.Instance.noAd == 1)
		{
		this.gameObject.GetComponent<Text>().text = "Ad free!";
		}
	}
}
