﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockLevel : UIObject {

  public UIPage shopPage;

	public void EnoughMoney()
	{
	}

	public void NotEnoughMoney()
	{
	}

    public virtual void OnPointerDown()
    {		
		UIDialogBox dialogBox = Instantiate(UIManager.Instance.YesNoDialog);
        dialogBox.PopUpDialog("Title", "Not enough gold to buy", OK, Cancel);
    }

    void OK()
    {
      UIManager.Instance.pageNavigation.navigationCommandsList.Add
      (
        new NavigationCommand(ref UIManager.Instance.pageNavigation, 
        UIManager.Instance.pageNavigation.currentPage, 
        shopPage, 
        NavigationAction.Next, 
        false)
      );
      
      UIManager.Instance.pageNavigation.navigationCommandsList.Do();

      
    }

    void Cancel()
    {
      Debug.Log("Cancel");
    }

}
