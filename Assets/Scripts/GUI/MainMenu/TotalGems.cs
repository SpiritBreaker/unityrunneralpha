﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalGems : UIObject {

	protected override void becomeVisible()
	{
		this.gameObject.GetComponent<Text>().text = Account.Instance.totalGems.ToString();
	}
}
