﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalCoins : UIObject {

	protected override void becomeVisible()
	{
		this.gameObject.GetComponent<Text>().text = Account.Instance.totalCoins.ToString();
	}
}
