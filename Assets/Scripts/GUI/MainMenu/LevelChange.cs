﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelChange : UIObject
{

    public int LevelID = 0;
    private int LevelsNumber;
    public Levels levels;
    public Text text;
    public Image previewImage;
    public GameObject unlockButton;
    public GameObject selectButton;
    public GameObject menuButtons;
    public Button leftButton;
    public Button rightButton;

    void Start()
    {
        //LevelsNumber = Gameplay.Instance.levels.Count;
        LevelID = levels.currentLevelId;
        LevelsNumber = levels.levels.Count;
        text.text = levels.GetLevelData(LevelID).name;

        SetButtonsActive();
    }

    public void Change()
    {
        text.text = levels.levels[LevelID].name;
        if (LevelID != levels.currentLevelId)
        {
            previewImage.sprite = Resources.Load<Sprite>(levels.GetLevelData(LevelID).previewImage);
            previewImage.enabled = true;
            menuButtons.SetActive(false);
            if (Account.Instance.levels[LevelID])
            {
                selectButton.SetActive(true);
                unlockButton.SetActive(false);
            }
            else
            {
                selectButton.SetActive(false);
                unlockButton.SetActive(true);
                unlockButton.GetComponentInChildren<Text>().text = "Unlock\n" + levels.GetLevelData(LevelID).goldPrice;
            }
        }

        else
        {
            previewImage.enabled = false;
            selectButton.SetActive(false);
            unlockButton.SetActive(false);
            menuButtons.SetActive(true);
        }

    }

    public void MakeCurrent(int id)
    {

    }

    public void SetButtonsActive()
    {
		Color inActiveColor = new Color(0.3f, 0.3f, 0.3f);

        if (LevelID == 0)
        {
            leftButton.enabled = false;
            leftButton.GetComponent<Image>().color = inActiveColor;
        }
        else
        {
            leftButton.enabled = true;
            leftButton.GetComponent<Image>().color = Color.white;
        }

        if (LevelID == LevelsNumber - 1)
        {
            rightButton.enabled = false;
            rightButton.GetComponent<Image>().color = inActiveColor;
        }
        else
        {
            rightButton.enabled = true;
            rightButton.GetComponent<Image>().color = Color.white;
        }
    }

    public void OnPointerDown(string arrow)
    {

        if (LevelsNumber < 2)
        {
            return;
        }

        if (arrow == "left")
        {
            if (LevelID != 0)
            {
                LevelID -= 1;
            }
            Change();
        }

        if (arrow == "right")
        {
            if (LevelID < LevelsNumber - 1)
            {
                LevelID += 1;
            }
            Change();
        }

        SetButtonsActive();
    }

}
