﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeFreezes : UIObject {

	protected override void becomeVisible()
	{
		this.gameObject.GetComponent<Text>().text = Account.Instance.timeFreezes.ToString();
	}
}
