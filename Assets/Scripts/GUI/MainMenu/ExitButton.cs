﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButton : MonoBehaviour {

    public virtual void OnPointerDown()
    {
      UIDialogBox dialogBox = Instantiate(UIManager.Instance.YesNoDialog);
      dialogBox.PopUpDialog("Title", "If you click\n you can exit the game", OK, Cancel);
    }


    void OK()
    {
      Debug.Log("Print OK");
    }

    void Cancel()
    {
      Debug.Log("Cancel");
    }

}
