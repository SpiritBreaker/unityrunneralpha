
import numpy as np
import matplotlib.pyplot as plt

def main():
    x = np.linspace(0, 15, 100)
    y = np.power(2, x)

    plt.figure()
    plt.plot(x, y)


    x = np.interp(3, x, y);

    plt.show()

if __name__ == '__main__':
    main()