﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//In the best case scenario this math has to be replaced by numerical math
//Math.NET Numerics for instance https://numerics.mathdotnet.com/ or 
// https://www.assetstore.unity3d.com/en/#!/content/77081

public interface IFunction
{
    float fx(float x);
}

public class ObstaclesFunction : IFunction
{
    public float fx(float x)
    {
        return Mathf.Pow(1F + (x * 20F), 0.6f);
    }
}

public class NormalDistribution
{
    static private float normpdf(float x, float mu, float sigma)
    {
        float u = (x-mu)/Mathf.Abs(sigma);
        float y = (1/(Mathf.Sqrt(2*Mathf.PI)*Mathf.Abs(sigma)))*Mathf.Exp(-u*u/2);
        return y;
    }

    static public float distribution(float x, float mu = 0f, float variance = 0.25f)
    {
        float sigma = Mathf.Sqrt(variance);
        return normpdf(x, mu, sigma);
    }
}


public static class WeightedChoise
{
    public static T Rand<T>(this IEnumerable<T> enumerable, Func<T, float> weightFunc)
    {
        float totalWeight = 0; // this stores sum of weights of all elements before current
        T selected = default(T); // currently selected element
        foreach (var data in enumerable)
        {
            float weight = weightFunc(data); // weight of current element
            float r = UnityEngine.Random.Range(0, totalWeight + weight); // random value
            if (r >= totalWeight) // probability of this is weight/(totalWeight+weight)
                selected = data; // it is the probability of discarding last selected element and selecting current one instead
            totalWeight += weight; // increase weight sum
        }

        return selected; // when iterations end, selected is some element of sequence. 
    }
}

public class Randomness : MonoBehaviour
{
    IFunction obstaclesFunction;

    List<int> x = new List<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    List<float> mask = new List<float>{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    Dictionary<int, float> foo = new Dictionary<int, float>();

    [SerializeField]
    public int random_value;

    float mu = 1f;
    float variance = 0.5f;

    public float weightFunc(int i)
    {
        return mask[i];
    }

    void Start()
    {
        obstaclesFunction = new ObstaclesFunction();
        foo.Add(0, 0);
        foo.Add(1, 0);
        foo.Add(2, 0);
        foo.Add(3, 0);
        foo.Add(4, 0);
        foo.Add(5, 0);
        foo.Add(6, 0);
        foo.Add(7, 0);
        foo.Add(8, 0);
        foo.Add(9, 0);
    }

    void Update()
    {
        mu = obstaclesFunction.fx(Time.time/100.0f);
        for (int i = 0; i < 10; i++)
        {
            mask[i] = NormalDistribution.distribution(x[i], mu, variance);
            foo[i] = mask[i];
        }

        random_value = (int)foo.Rand(t => t.Value).Key;
    }

}
