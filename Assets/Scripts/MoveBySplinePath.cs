﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBySplinePath : MonoBehaviour
{

    public ISpline Spline;
    public float startPosition;
    public float speed;
    public int direction;

    float t = 0.0f;
    int previous_t = -1;

    Vector3 p0, p1, p2, p3;
    Vector3 tangent, normal, binormal;

    private GameObject Actor;
    public float angle;

    void Start()
    {
		Actor = this.gameObject;
        Spline = CompositionRoot.Instance.spline;
		startPosition =  (float)(Actor.GetComponent<InteractiveObject>().point.i + Actor.GetComponent<InteractiveObject>().point.t);
		t = startPosition;

        this.angle = Actor.GetComponent<InteractiveObject>().angle;
        Spline = CompositionRoot.Instance.spline;
    }


    void get_p_c()
    {
        if ((int)t > Spline.AmountOfPoints - 1)
        {
            t = 0.0f;
        }

        p0 = Spline.GetKnot((int)MyMath.mod((int)t - 1, Spline.AmountOfPoints)).position;
        p1 = Spline.GetKnot((int)MyMath.mod((int)t, Spline.AmountOfPoints)).position;
        p2 = Spline.GetKnot((int)MyMath.mod((int)t + 1, Spline.AmountOfPoints)).position;
        p3 = Spline.GetKnot((int)MyMath.mod((int)t + 2, Spline.AmountOfPoints)).position;
    }

    void Update()
    {
        t += speed * Time.deltaTime;

        if ((int)t != previous_t)
        {
            previous_t = (int)t;
            get_p_c();
        }

        tangent = Spline.GetTangent(t % 1, p0, p1, p2, p3).normalized;
        normal = Vector3.Cross(tangent, Actor.transform.up).normalized;
        binormal = Vector3.Cross(normal, tangent).normalized;
        tangent = Vector3.Cross(binormal, normal).normalized;

        Actor.transform.localPosition = Spline.GetWorldPosition(t % 1, p0, p1, p2, p3) + (Actor.transform.up * Actor.GetComponent<InteractiveObject>().height);

        Actor.transform.rotation = Quaternion.LookRotation(tangent);
        Actor.transform.Rotate(tangent, angle, Space.World);


    }
}
