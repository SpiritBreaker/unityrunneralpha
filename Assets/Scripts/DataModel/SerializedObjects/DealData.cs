﻿using System.Collections;
using System.Collections.Generic;
using DataModel;
using System.Reflection;
using System;

[Serializable]
public class DealData : SerializableObject
{

    public int itemId;
    public int amount;
    public Currency currency;
    public float price;

    public override void Compare()
    {
    }
}
