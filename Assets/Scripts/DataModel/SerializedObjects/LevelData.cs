﻿using System.Collections;
using System.Collections.Generic;
using DataModel;
using System.Reflection;
using System;

[Serializable]
public class LevelData : SerializableObject
{
    public string splineMeshPrefab;
    public string name;
    public string prefabName;
    public string previewImage;
    public float goldPrice;

    public override void Compare()
    {
    }
}