﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levels : MonoBehaviour {

	public List<LevelData> levels = new List<LevelData>();
	GameObject trackPrefab;
	public int currentLevelId;

	// Use this for initialization
	void Awake () 
	{
		levels = DataManager.Instance.QueryGameData<LevelData>();
	}

	void Start()
	{
		LoadLevel(0);
	}

	public LevelData GetLevelData(int id)
	{
		return levels[id];
	}

	void LoadLevel(int id)
	{
		currentLevelId = id;
		//trackPrefab = Instantiate(Resources.Load(levels[id].splineMeshPrefab, typeof(GameObject))) as GameObject;
	}

	void Unload()
	{
		Object.Destroy(this.trackPrefab);
	}

	void ChangeLevel(int id)
	{
		Unload();
		LoadLevel(id);
	}

}
