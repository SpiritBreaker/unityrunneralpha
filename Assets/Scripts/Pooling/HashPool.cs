﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading;

public class HashPool
{
    public int maxInstances;
    public int instanceCount;
    public GameObject prefab;
    public HashSet<GameObject> buffer;
    public bool isLocked = false;

    public HashPool(GameObject prefab)
    {
        this.prefab = prefab;
        this.buffer = new HashSet<GameObject>();
        this.instanceCount = 0;
    }
}

class PoolMember : MonoBehaviour
{
    private string _poolName;
    private GameObject _prefab;
    public string poolName { get { return _poolName;} set { _poolName = value; } }
    public GameObject prefab { get { return _prefab; } set { _prefab = value; } }
}

public class HashPooling
{
    public Dictionary<GameObject, HashPool> DeactivePools;
    public Dictionary<GameObject, HashPool> ActivePools;

    void Init(GameObject prefab = null)
    {
        //Check if pool have already exist
        //if pools empty create a new one
        if (DeactivePools == null)
        {
            DeactivePools = new Dictionary<GameObject, HashPool>();
            ActivePools = new Dictionary<GameObject, HashPool>();
        }

        //if pools not empty check if there is a pool exist 
        //with the passing object
        if (prefab != null && DeactivePools.ContainsKey(prefab) == false)
        {
            DeactivePools[prefab] = new HashPool(prefab);
            ActivePools[prefab] = new HashPool(prefab);
        }
    }

     public  GameObject Spawn(GameObject prefab)
    {
        GameObject obj;
        Init(prefab);

        if (DeactivePools[prefab].buffer.Count == 0)
        {
            //Debug.Log("DeactivePools[prefab].buffer.Count == 0");
            obj = (GameObject)GameObject.Instantiate(prefab);
            ActivePools[prefab].buffer.Add(obj);
            obj.AddComponent<PoolMember>().prefab = prefab;
        }
        else 
        {
            //Debug.Log("else");
            obj = DeactivePools[prefab].buffer.First();
            DeactivePools[prefab].buffer.Remove(obj);
            ActivePools[prefab].buffer.Add(obj);           
        }
        obj.SetActive(true);
        return obj;
    }

     public GameObject Despawn(GameObject obj)
    {
        GameObject prefab = obj.GetComponent<PoolMember>().prefab;
        ActivePools[prefab].buffer.Remove(obj);
        DeactivePools[prefab].buffer.Add(obj);

        obj.SetActive(false);
        return obj;
    }

     public GameObject transferTo( GameObject obj, Dictionary<GameObject, HashPool> pools)
    {
        GameObject prefab = obj.GetComponent<PoolMember>().prefab;
        pools[prefab].buffer.Add(obj);
        obj.SetActive(false);
        return obj;
    }
}