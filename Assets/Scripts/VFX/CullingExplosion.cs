﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CullingExplosion : MonoBehaviour {

    #region Singleton
    private static CullingExplosion _instance;
    public static CullingExplosion Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<CullingExplosion>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("CullingExplosion");
                    _instance = container.AddComponent<CullingExplosion>();
                }
            }

            return _instance;
        }
    }
    #endregion

	BoxCollider cullingCollider;
	float initialScale;
	float size;

	// Use this for initialization
	void Start () {
		cullingCollider = this.gameObject.GetComponent<BoxCollider>();
		cullingCollider.enabled = false;
		initialScale = cullingCollider.size.x;
	}

	IEnumerator explosion()
	{
		while(true)
		{
			if (size < initialScale + 100f)
			{
				size += 500f * Time.deltaTime;
				cullingCollider.size = new Vector3(size, size, size);
				yield return null;
			}
			else 
			{
				cullingCollider.size = new Vector3(initialScale, initialScale, initialScale);
				cullingCollider.enabled = false;
				size = initialScale;
				yield break;
			}
		}
	}

	public void explode()
	{
		cullingCollider.enabled = true;
		StartCoroutine(explosion());
	}

	// Update is called once per frame
	void Update () {
		
	}
}
