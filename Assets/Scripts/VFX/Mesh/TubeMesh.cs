﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Assuming you want to extrude by a distance z, you need to follow these steps:

// 0) let n be the original number of vertices (4 in your example)

// 1) For each vertex in your vertex array, add (0,0,z) to it, and add the result to your vertex array,
// for a total of 2*n vertices. So, for your example, you will add the vertices (0,0,z), (10,0,z),
// (10,10,z), (0,10,z) to your vertex array, for a total of 2*4=8 vertices.

// 2) Create a list of boundary (as opposed to internal) edges for your original mesh. 
// To do this, create a list of all triangle edges (3 edges going in clockwise order for each triangle). 
// Then remove pairs of equal but opposite edges (these are the internal edges). 
// For your example, you will start with 6 edges, and end up with 4 edges after
// removing the edge pair (3,1) and (1,3).

// 3) for each triangle (a,b,c) in your triangle list, create a corresponding triangle 
// (a+n,b+n,c+n). These will be the extruded faces

// 4) Finally, you want to create the sides of your extruded shape. For each edge (a,b)
// in the boundary edge list you created in step 2, add the triangles (a,b,b+n) and (b+n,a+n,a)

// That's it. Assuming no typos on my part, and no typos on your part, you should now have your desired mesh.

//http://ltcconline.net/greenl/courses/203/Vectors/changeOfBasis.htm

[RequireComponent(typeof(MeshRenderer))]
public class TubeMesh : MonoBehaviour
{

    [SerializeField]
    public int n = 36;
    [SerializeField]
    public float radius = 2f;
    [SerializeField]
    public float step = 1f;
    int shift = 0;

    ISpline spline;

    List<Vector3> vertices = new List<Vector3>();
    List<Vector3> normals = new List<Vector3>();
    List<Vector2> uv = new List<Vector2>();
    List<Edge> edges = new List<Edge>();
    List<Quad> quads = new List<Quad>();
    List<int> indexes = new List<int>();


    // Use this for initialization
    void Start()
    {
        spline = this.GetComponent<Spline>().spline;
        ProceduralTube();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public class Edge
    {
        public Edge()
        {
        }
        public Edge(int v1, int v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }

        public int v1;
        public int v2;
    }

    public class Quad
    {
        public Quad()
        {
        }
        public Quad(Edge edge1, Edge edge2)
        {
            this.edge1 = edge1;
            this.edge2 = edge2;
        }

        public Edge edge1;
        public Edge edge2;


    }

    public class Triangle
    {
        public Triangle()
        {
        }
        public Triangle(int v1, int v2, int v3)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
        }
        public int v1;
        public int v2;
        public int v3;
    }

    Triangle[] Triangulate(Quad quad)
    {
        int v1, v2, v3, v4;
        v1 = quad.edge1.v1;
        v2 = quad.edge1.v2;

        v3 = quad.edge2.v1;
        v4 = quad.edge2.v2;

        return new Triangle[2] { new Triangle(v3, v2, v1), new Triangle(v3, v4, v2) };
    }

    void Extrude()
    {
        //extrude edges
        //add new virtices
        for (int i = 0; i < n; i++)
        {
            vertices.Add(new Vector3());
            normals.Add(new Vector3());
            uv.Add(new Vector2());
        }

        for (int i = 0; i < n - 1; i++)
        {
            Edge newEdge = new Edge((int)MyMath.mod((n * shift) + i, vertices.Count), (int)MyMath.mod((n * shift) + i + 1, vertices.Count));
            edges.Add(newEdge);
        }

        // add closing edge
        edges.Add(new Edge(vertices.Count - 1, n * shift));

        for (int i = 0; i < n; i++)
        {
            quads.Add(new Quad(edges[(n * (shift - 1)) + i], edges[(n * shift) + i]));
        }

        //Create indexing
        for (int i = 0; i < n; i++)
        {
            Triangle[] tris = Triangulate(quads[(n * (shift - 1)) + i]);
            indexes.Add(tris[0].v1);
            indexes.Add(tris[0].v2);
            indexes.Add(tris[0].v3);

            indexes.Add(tris[1].v1);
            indexes.Add(tris[1].v2);
            indexes.Add(tris[1].v3);

        }

    }

    Quaternion GetOrientation3D(SplinePoint point)
    {
        return Quaternion.LookRotation(point.tangent, point.binormal);
    }

    void SetSplinePosition(Quaternion rotation, Vector3 position)
    {
        for (int i = 0; i < n; i++)
        {
            // Calculate the angle of the corner in radians.
            float cornerAngle = 2f * Mathf.PI / (float)n * i;
            // Get the X and Y coordinates of the corner point.
            Vector3 currentCorner = new Vector3(Mathf.Cos(cornerAngle) * radius, 0, Mathf.Sin(cornerAngle) * radius);
            normals[(n * shift) + i] = rotation * (Quaternion.Euler(90, 0, 0) * currentCorner);
            vertices[(n * shift) + i] = position + rotation * (Quaternion.Euler(90, 0, 0) * currentCorner);
            if (shift % 2 == 0)
            {
                if (i % 2 == 0)
                {
                    uv[(n * shift) + i] = new Vector2(0f, 1f);
                }
                else
                {
                     uv[(n * shift) + i] = new Vector2(1f, 1f);
                }
            }
            else
            {
                if (i % 2 == 0)
                {
                     uv[(n * shift) + i] = new Vector2(0f, 0f);
                }
                else
                {
                     uv[(n * shift) + i] =new Vector2(1f, 0f);
                }
            }
        }
    }

    void ProceduralTube()
    {
        MeshFilter filter = gameObject.AddComponent<MeshFilter>();
        Mesh mesh = filter.mesh;
        mesh.Clear();

        //generate initial edges
        for (int i = 0; i < n; i++)
        {
            vertices.Add(new Vector3());
            normals.Add(new Vector3());
            if (i % 2 == 0)
            {
                uv.Add(new Vector2(0f, 0f));
            }
            else
            {
                uv.Add(new Vector2(1f, 0f));
            }
        }

        for (int i = 0; i < n; i++)
        {
            edges.Add(new Edge((int)MyMath.mod(i, n), (int)MyMath.mod(i + 1, n)));
        }

        Vector3 binormal = Vector3.up;
        SplinePoint point = spline.GetPoint(0, 0);
        SetSplinePosition(GetOrientation3D(point), point.position);

        for (int i = 0; i < spline.AmountOfPoints+1; i++)
        {
            for (float t = 0.0f; t < 1; t += step)
            {
                shift++;
                Extrude();
                point = spline.GetPoint(i, t);
                SetSplinePosition(GetOrientation3D(point), point.position);
            }
        }


        


        mesh.vertices = vertices.ToArray();
        mesh.triangles = indexes.ToArray();
        mesh.normals = normals.ToArray();
        mesh.uv = uv.ToArray();
        mesh.RecalculateBounds();
    }
}
