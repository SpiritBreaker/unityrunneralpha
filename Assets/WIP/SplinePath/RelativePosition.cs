﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RelativePosition : MonoBehaviour
{

    [SerializeField]
    public TileMap map;
    [SerializeField]
    public Vector3 debugPosition;

    public float mapWidth;
    public float mapHeight;

    SplinePathCreator pathCreator;
    SplinePath path;

    [SerializeField]
    public List<Vector3> points;
    [SerializeField]
    public Vector3[] temp;



    Vector3 ConvertToMapSpace(Vector3 pos)
    {
        mapWidth = map.Columns * map.TileWidth;
        mapHeight = map.Rows * map.TileHeight;
        var position = map.transform.position;

        Vector3 newPos = new Vector3();

        newPos.x = (pos.x - position.x) / (position.x + mapWidth - position.x);
        newPos.x = Mathf.Clamp(newPos.x, 0, 1);

        newPos.z = (pos.z - position.z) / (position.z + mapHeight - position.z);
        newPos.z = Mathf.Clamp(newPos.z, 0, 1);

        return newPos;

    }

    void Start()
    {

        pathCreator = this.gameObject.GetComponent<SplinePathCreator>();
        path = pathCreator.path;
    }

    // Update is called once per frame
    void Update()
    {
        debugPosition = ConvertToMapSpace(this.transform.position);
        temp = path.CalculateEvenlySpacedPoints(0.5f, 1f);

        for (int i = 0; i < temp.Length; i++)
        {
            temp[i] = ConvertToMapSpace(this.transform.TransformPoint((temp[i])));
        }

    }

}
