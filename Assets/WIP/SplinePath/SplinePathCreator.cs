﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SplinePathCreator : MonoBehaviour
{

    [HideInInspector]
    public SplinePath path;

    public Color anchorColor = Color.red;
    public Color controlCol = Color.white;
    public Color segmentCol = Color.green;
    public Color selectedSegmentCol = Color.yellow;

    public float anchorDiameter = .1f;
    public float controlDiamter = .075f;
    public bool displayControlPoints = true;

    public void CreatePath()
    {
        path = new SplinePath(this.transform.TransformDirection(Vector3.zero));
    }

    void Reset()
    {
        CreatePath();
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (UnityEditor.Selection.activeGameObject == this.gameObject) return;
        for (int i = 0; i < path.NumPoints; i++)
        {
            if (i % 3 == 0)
            {
                Vector3 point = this.transform.TransformPoint(path[i]);
                Gizmos.color = new Color(1f, 0.0f, 0.0f);
                Gizmos.DrawSphere(point, 0.2f);
            }
        }

        Vector3[] bezier = path.CalculateEvenlySpacedPoints(1, 1f);
        for (int i = 0; i < bezier.Length - 1; i++)
        {
            Gizmos.color = new Color(1f, 1f, 0.0f);
            Gizmos.DrawLine( this.transform.TransformPoint(bezier[i]),  this.transform.TransformPoint(bezier[i+1]));
        }
    }
#endif

}
