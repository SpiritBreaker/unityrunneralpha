﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TileMapType
{
    Level1, Level2, Level3, Level4, Level5, Level6, Level7, Level8, Level9, Level10
}

public class SplineGameTile : MonoBehaviour
{
    public string Name;

    public List<Vector3> localVec3Pos = new List<Vector3>();
    public List<GameObject> inGameObject = new List<GameObject>();
    public List<double> localPosition = new List<double>();
    public List<Vector3> localScale = new List<Vector3>();
    public List<Quaternion> localRotation = new List<Quaternion>();
    public List<float> angle = new List<float>();

    public double beginPos;
    public double endPos;

    public double length;
    public TileMapType type;

    public List<GameObject> prefabs = new List<GameObject>();

    public List<Vector3> coinsSpline = new List<Vector3>();
    public List<double> coinsLocalPosition = new List<double>();
    public List<float> coinsAngle = new List<float>();
    public GameObject coinsPrefab;
}



static public class TileMapAdapter
{
    static public SplineGameTile AdaptTileMap(TileMap map)
    {

        SplineGameTile tile = new SplineGameTile();
        
        tile.Name = map.gameObject.name;

        foreach (Transform child in map.GetComponent<Transform>().Find("Obstacle"))
        {
            if (child.GetComponent<TileMapObject>() != null)
            {
                Vector3 temp = ConvertToMapSpace(child.position, map);

                tile.inGameObject.Add(child.GetComponent<TileMapObject>().prefab);
                tile.length = map.Rows * map.TileHeight;
                tile.localPosition.Add(Mathf.Lerp(0, (float)tile.length, temp.z));
                tile.localVec3Pos.Add(child.position);
                tile.localScale.Add(child.localScale);
                tile.localRotation.Add(child.rotation);
                tile.angle.Add(Mathf.Lerp(360, 0, temp.x));
                tile.prefabs.Add(child.GetComponent<TileMapObject>().prefab);
            }
        
        }


        foreach (Transform child in map.GetComponent<Transform>().Find("Reward"))
        {
            if (child.GetComponent<SplinePathCreator>() != null)
            {

                SplinePathCreator pathCreator;
                SplinePath path;

                pathCreator = child.gameObject.GetComponent<SplinePathCreator>();
                path = pathCreator.path;

                Vector3[] temp = path.CalculateEvenlySpacedPoints(2f, 2f);
                for (int i = 0; i < temp.Length; i++)
                {

                    temp[i] = ConvertToMapSpace(child.TransformPoint((temp[i])), map);

                    tile.coinsSpline.Add(temp[i]);

                    tile.coinsLocalPosition.Add(Mathf.Lerp(0, (float)tile.length, temp[i].z));

                    tile.coinsAngle.Add(Mathf.Lerp(360, 0, temp[i].x));

                    tile.coinsPrefab = child.gameObject.GetComponent<TileMapObject>().prefab;
                }


                //tile.coinsSpline.Add()
            }
        }
        return tile;
    }

    static public Vector3 ConvertToMapSpace(Vector3 pos, TileMap map)
    {
        float mapWidth = map.Columns * map.TileWidth;
        float mapHeight = map.Rows * map.TileHeight;
        var position = map.transform.position;

        Vector3 newPos = new Vector3();

        newPos.x = (pos.x - position.x) / (position.x + mapWidth - position.x);
        newPos.x = Mathf.Clamp(newPos.x, 0, 1);

        newPos.z = (pos.z - position.z) / (position.z + mapHeight - position.z);
        newPos.z = Mathf.Clamp(newPos.z, 0, 1);

        return newPos;
    }
    /// <summary>
    /// Get Local cell distance.  0.0 distance is row number 0; 
    /// </summary>
    /// <param name="map">TileMap instance</param>
    /// <param name="row">Row number</param>
    /// <returns></returns>
    public static double GetLocalCellDistance(TileMap map, int row)
    {
        return map.TileHeight * row;
    }
}

