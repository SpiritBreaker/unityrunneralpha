﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMapFix : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 20; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                var objectsWithTag = GameObject.Find(string.Format("Tile_{0}_{1}", i, j));
                Destroy(objectsWithTag);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
