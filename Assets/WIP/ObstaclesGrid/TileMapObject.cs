﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMapObject : MonoBehaviour
{
    public TileMap map;
    public GameObject prefab;

    public TileMapObject(GameObject obj, TileMap map, int row, int column)
    {
        this.map = map;
    }
}
