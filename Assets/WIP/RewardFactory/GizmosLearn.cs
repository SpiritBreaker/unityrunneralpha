﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmosLearn : MonoBehaviour
{

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, 1f);

        Gizmos.DrawWireSphere(transform.position + Vector3.left, 1f);
    }

    /// <summary>
	/// Callback to draw gizmos only if the object is selected.
	/// </summary>
	void OnDrawGizmosSelected()
	{    
 
	}

}
