﻿using UnityEngine;
using System.Collections;

public class TestGizmos : MonoBehaviour
{


    public float spacing = .1f;
    public float resolution = 1;

    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Vector3[] points = FindObjectOfType<SplinePathCreator>().path.CalculateEvenlySpacedPoints(spacing, resolution);
        Transform tr = FindObjectOfType<SplinePathCreator>().transform;
        foreach (Vector3 p in points)
        {
            // GameObject g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            // g.transform.position = p;
            // g.transform.localScale = Vector3.one * spacing * .5f;

            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(tr.TransformPoint(p), 0.02f);
        }
    }
}