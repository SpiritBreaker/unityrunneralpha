﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GlobalShader : MonoBehaviour {
	

	// Use this for initialization
	void Start () {
		Color TintColor = RenderSettings.skybox.GetColor("_Tint");
		float Exposure = RenderSettings.skybox.GetFloat("_Exposure");
		Texture tex = RenderSettings.skybox.GetTexture("_Tex");
		
		Shader.SetGlobalColor("_Tint", TintColor);
		Shader.SetGlobalFloat("_Exposure", Exposure);
		Shader.SetGlobalTexture("_SkyCubemap", tex);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
