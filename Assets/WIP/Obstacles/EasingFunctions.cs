﻿using UnityEngine;


public static class EasingFunctions 
{

    public static float linear(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return x;
    }

    public static float inPow(float x, float p)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return Mathf.Pow(x, p);
    }

    public static float outPow(float x, float p)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;

        int sign = p % 2 == 0 ? -1 : 1;
        return (sign * (Mathf.Pow(x - 1f, p) + sign));
    }

    public static float inOutPow(float x, float p)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;

        x *= 2;
        if (x < 1) return inPow(x, p) / 2;

        int sign = p % 2 == 0 ? -1 : 1;
        return (sign / 2.0f * (Mathf.Pow(x - 2f, p) + sign * 2f));
    }

    public static float inSin(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return -Mathf.Cos(x * (Mathf.PI / 2.0f)) + 1;
    }

    public static float outSin(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return Mathf.Sin(x * (Mathf.PI / 2.0f));
    }

    public static float inOutSin(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return -0.5f * (Mathf.Cos(Mathf.PI * x) - 1.0f);
    }

    public static float inExp(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return Mathf.Pow(2.0f, 10.0f * (x - 1.0f));
    }

    public static float outExp(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return -Mathf.Pow(2, -10 * x) + 1;
    }

    public static float inOutExp(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return x < 0.5f ? 0.5f * Mathf.Pow(2f, 10f * (2f * x - 1f)) :
           0.5f * (-Mathf.Pow(2f, 10f * (-2f * x + 1f)) + 2f);
    }

    public static float inCirc(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return -(Mathf.Sqrt(1f - x * x) - 1f);
    }

    public static float outCirc(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return Mathf.Sqrt(1f - (x - 1f) * (x - 1f));
    }

    public static float inOutCirc(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return x < 1f ? -0.5f * (Mathf.Sqrt(1f - x * x) - 1f) :
           0.5f * (Mathf.Sqrt(1f - ((2f * x) - 2f) * ((2f * x) - 2f)) + 1f);
    }

    public static float rebound(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;

        if (x < (1f / 2.75f)) return 1f - 7.5625f * x * x;
        else if (x < (2f / 2.75f)) return 1f - (7.5625f * (x - 1.5f / 2.75f) *
            (x - 1.5f / 2.75f) + 0.75f);
        else if (x < (2.5f / 2.75f)) return 1f - (7.5625f * (x - 2.25f / 2.75f) *
            (x - 2.25f / 2.75f) + 0.9375f);
        else return 1f - (7.5625f * (x - 2.625f / 2.75f) * (x - 2.625f / 2.75f) +
           0.984375f);
    }

    public static float inBack(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return x * x * ((1.70158f + 1f) * x - 1.70158f);
    }

    public static float outBack(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        //return (x - 1f) * (x - 1f) * ((1.70158f + 1f) * (x - 1f) + 1.70158f) + 1f;
		return (x - 0.38f) * (x - 0.4f) * ((3.1f + 5.9f) * (x - 1f) + 1.70158f) + 1f;
    }

    public static float inOutBack(float x)
    {
        if (x < 0.0f) return 0.0f;
        if (x > 1.0f) return 1.0f;
        return x < 0.5f ? 0.5f * (4f * x * x * ((2.5949f + 1f) * 2f * x - 2.5949f)) :
           0.5f * ((2f * x - 2f) * (2f * x - 2f) * ((2.5949f + 1f) * (2f * x - 2f) +
           2.5949f) + 2f);
    }

    public static float impulse(float x, float k) //k controls the stretching of the func
    {
        float h = k * x;
        return h * Mathf.Exp(1.0f - h);
    }

    public static float expPulse(float x, float k, float n)
    {
        return Mathf.Exp(-k * Mathf.Pow(x, n));
    }

}