﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesAnimation : MonoBehaviour
{

	public float time = 0f;
	public float height = 0f;
	public float maxHeight = 0f;
	public float minHeight = 10f;

	public AnimationCurve curve;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        time = MyMath.mod(Time.fixedTime, 8.0f); //loop every 8 sec

        // height = (1f - EasingFunctions.outBack(time / 2.0f) ) * //ascend for 2 sec after 4 sec
        //               (maxHeight - minHeight) + minHeight;

		height = curve.Evaluate(time/3f) * (maxHeight - minHeight) + minHeight;

		this.transform.position = new Vector3(transform.position.x, height, transform.position.z);
    }
}
