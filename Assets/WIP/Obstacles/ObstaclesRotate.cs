﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesRotate : MonoBehaviour {

	public float time;
	float easingPrevious;
	// Use this for initialization
	void Start () {
		
	}
	
	IEnumerator RotateAnimation()
	{

		yield return null;
	}

	// Update is called once per frame
	void Update () {

		time = MyMath.mod(Time.fixedTime, 0.7f);
		this.transform.Rotate(this.transform.forward,( EasingFunctions.inOutBack(time) * 180f) - easingPrevious, Space.World);
		easingPrevious = EasingFunctions.inOutBack(time) * 180f;
		
	}
}
