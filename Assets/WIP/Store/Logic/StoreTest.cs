﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;
using OnlineStore;



public class Inventory
{

}

// MonoBehaviour testCase ----------------------------
public class NewPlayer : ICustomer
{

    BankAccount account;
    Inventory inventory;

    public BankAccount Account
    {
        get { return account; }
        set { }
    }
    public Inventory Inventory
    {
        get { return inventory; }
        set { }
    }

    public NewPlayer()
    {
        account = new BankAccount();
        inventory = new Inventory();
    }

    public OnlineStore.MStatus ReceiveAnOrder(OnlineStore.Order order)
    {
        return null;
    }

}

public class StoreTest : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        NewPlayer player = new NewPlayer();
        player.Account.balance[Currency.USD] = 100.0f;
        Store store = this.GetComponent<Store>();

        Debug.Log("-------------Purchase in store--------------");

        Debug.Log("--Customer Account:");
        foreach (KeyValuePair<Currency, float> kvp in player.Account.balance)
        {
            Debug.LogFormat("{0}, {1}",
                kvp.Key, kvp.Value);
        }
        Debug.Log("--Store Account   :");
        foreach (KeyValuePair<Currency, float> kvp in store.Account.balance)
        {
            Debug.LogFormat("{0}, {1}",
                kvp.Key, kvp.Value);
        }

        Debug.Log("---------------Start purchase------------------");
        store.OrderRequest(player, store.deals[0]);
        store.CompleteOrders();
        Debug.Log("---------------Purchase proceed----------------");
        Debug.Log("--Customer Account:");
        foreach (KeyValuePair<Currency, float> kvp in player.Account.balance)
        {
            Debug.LogFormat("{0}, {1}",
                kvp.Key, kvp.Value);
        }
        Debug.Log("--Store Account   :");
        foreach (KeyValuePair<Currency, float> kvp in store.Account.balance)
        {
            Debug.LogFormat("Key = {0}, Value = {1}",
                kvp.Key, kvp.Value);
        }
        Debug.Log("--------------------------------------------");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
