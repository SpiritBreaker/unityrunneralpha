﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataModel;
//https://stackoverflow.com/questions/43121007/design-patterns-for-a-supermarket-system
using OnlineStore;

namespace OnlineStore
{

    #region MStatus

    /// <summary>
    /// Enumerator equivalent result of any store operations
    /// kSuccess - operation done with success
    /// kFailure - operation failed to perform, with no clear explanation
    /// kNotEnoughCurrency -client dosen't have necessary amount of currency on its account
    /// kConnectionTimeOut -client wait to much of result preformed operation
    /// kMissingCurrent -client dosen't have ability to pay that current at all
    /// </summary>
    public enum MStatusCode
    {
        kSuccess, kFailure, kNotEnoughCurrency, kConnectionTimeOut, kMissingCurrency
    }

    /// <summary>
    /// Class that contain status of any client-store operation
    /// </summary>
    public class MStatus
    {
        /// <summary>
        /// To create MStatus feedback statusCode string must be injected, 
        /// error string the will be returned based on given status code
        /// </summary>
        /// <param name="statusCode"></param>
        public MStatus(MStatusCode statusCode)
        {
            this.statusCode = statusCode;
            if (statusCode == MStatusCode.kNotEnoughCurrency)
            {
                errorString = "Customer dosn't have enough currency to make a purchase";
            }
            if (statusCode == MStatusCode.kConnectionTimeOut)
            {
                errorString = "Can't connect to online store";
            }
            if (statusCode == MStatusCode.kMissingCurrency)
            {
                errorString = "Currency dosen't exist on Account";
            }
        }
        readonly public MStatusCode statusCode;
        readonly public string errorString;
    }
    #endregion

    /// <summary>
    /// Interface that allow to become a customer. Customer has an ablity to perform 3 operations
    /// -Withdraw Currency from it's account
    /// -Deposit Currency to account
    /// -Perform operations on received oreder, such as placing items from list 
    /// on their positions
    /// </summary>
    #region Customer
    public interface ICustomer
    {
        //MStatus Withdraw(Currency currency, float amount);
        //MStatus Deposit(Currency currency, float amount);
        BankAccount Account { get; set; }
        Inventory Inventory { get; set; }
        MStatus ReceiveAnOrder(Order order);
    }
    #endregion


    public class Deal
    {
        public readonly int itemId;
        public readonly int amount;
        public readonly Currency currency;
        public readonly float price;

        public Deal(int itemId, int amount, Currency currency, float price)
        {

            this.itemId = itemId;
            this.amount = amount;
            this.currency = currency;
            this.price = price;
        }
    }

    #region Order
    public class Order
    {
        public readonly ICustomer customer;
        public readonly Deal deal;

        public Order(ICustomer customer, Deal deal)
        {
            if (customer == null)
            {
                throw new System.ArgumentNullException("To create an order customer need to be known");
            }

            if (deal == null)
            {
                throw new System.ArgumentNullException("To create an order deal need be known");
            }

            this.customer = customer;
            this.deal = deal;
        }
    }
    #endregion

    #region POSTerminal
    public class POSTerminal
    {
        public MStatus Transaction(BankAccount customerAccount, BankAccount storeAccount, Currency currency, float amount)
        {
            Debug.LogFormat("New transaction started of order: {0}");
            CommandList commands = new CommandList()
            {
                new Command(ref customerAccount, Action.withdraw, currency,  amount),
                new Command(ref storeAccount, Action.deposit, currency, amount)
            };


            bool resultWithdraw = commands[0].call();
            if (resultWithdraw)
            {
                commands[1].call();
                Debug.Log("Transaction succeed");
                return new MStatus(MStatusCode.kSuccess);
            }
            else
            {
                Debug.Log("Transaction failed");
                return new MStatus(MStatusCode.kFailure);
            }
        }

    }

    #endregion
}


public interface IStore
{
    BankAccount Account
    {
        get;
        set;
    }

    bool PlaceOrder(ICustomer customer, Deal deal);
    bool OrderRequest(ICustomer customer, Deal deal);
    MStatus CompleteOrder(Order order);
}

#region Store
public class Store : MonoBehaviour, IStore
{
    private BankAccount account;
    public readonly List<Deal> deals = new List<Deal>();

    public BankAccount Account
    {
        get { return account; }
        set { }
    }
    private List<Order> orders = new List<Order>();

    public void Awake()
    {

        List<DealData> dealData = DataManager.Instance.QueryGameData<DealData>();
        if (dealData != null && dealData.Count > 0)
        {
            for (int i = 0; i < dealData.Count; i++)
            {
                deals.Add(new Deal(dealData[i].itemId, dealData[i].amount, dealData[i].currency, dealData[i].price));
            }
        }


        account = new BankAccount();
        orders = new List<Order>();
    }


    // public readonly List<Deal> deals = new List<Deal>()
    // {
    //     new Deal(1, 10, Currency.USD, 2.99f),
    //     new Deal(2, 25, Currency.Coin, 100f),
    //     new Deal(3, 1, Currency.Gem, 10f),
    //     new Deal(4, 1, Currency.AdToken, 1f)
    // };

    // bool CreateDeal()
    // {
    //     return false;
    // }

    public bool PlaceOrder(ICustomer customer, Deal deal)
    {
        orders.Add(new Order(customer, deal));
        Debug.LogFormat("Order placed id: {0}", orders.Count - 1);
        return true;
    }

    public bool OrderRequest(ICustomer customer, Deal deal)
    {
        Debug.LogFormat("Order request recieved: itemId {0} of {1} for {2}{3}", deal.amount, deal.itemId, deal.currency, deal.price);
        PlaceOrder(customer, deal);
        return true;
    }

    public MStatus CompleteOrder(Order order)
    {
            Debug.LogFormat("Starting to make proccess order : id{0}");
            MStatus status;
            POSTerminal termimal = new POSTerminal();
            status = termimal.Transaction(order.customer.Account, this.Account, order.deal.currency, order.deal.amount);
            return status;
    }

    public void CompleteOrders()
    {
        MStatus status;
        for (int i = 0; i < orders.Count; i++)
        {
            status = CompleteOrder(orders[i]);
            if (status.statusCode == MStatusCode.kSuccess)
            {
                orders[i].customer.ReceiveAnOrder(orders[i]);
                Debug.Log("the order processed successfully");
            }
            else
            {
                Debug.Log("the order is not passed");
            }
        }
    }
}
#endregion