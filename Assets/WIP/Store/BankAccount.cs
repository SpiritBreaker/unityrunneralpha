﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public enum Currency { USD, Coin, Gem, AdToken }

public class Bank
{
}

[Serializable]
public class BankAccount
{
    private int id;
    public readonly Dictionary<Currency, float> balance = new Dictionary<Currency, float>();
    float overdfraft_limit = 0.0f;

    public BankAccount()
    {
        balance.Add(Currency.USD, 0f);
        balance.Add(Currency.Coin, 0f);
        balance.Add(Currency.Gem, 0f);
        balance.Add(Currency.AdToken, 0f);
    }

    public bool deposit(Currency currency, float amount)
    {
        balance[currency] += amount;
        Debug.Log("deposited " + amount + ", balance now " + balance[currency]);
        return true;
    }

    public bool withdraw(Currency currency, float amount)
    {
        if (balance[currency] - amount >= overdfraft_limit)
        {
            balance[currency] -= amount;
            Debug.Log("withdrew " + amount + ", balance now " + balance[currency]);
            return true;
        }

        return false;
    }
}

public enum Action { deposit, withdraw };

interface ICommand
{
    bool call();
    void undo();
}


class CommandList : List<Command>, ICommand
{
    public bool call()
    {
        for (int i = 0; i < this.Count; i++)
        {
            this[i].call();
        }

        return true;
    }

    public void undo()
    {
        for (int i = this.Count - 1; i >= 0; i--)
        {
            this[i].undo();
        }
    }
}

class Command : ICommand
{
    BankAccount account;
    float amount;
    Currency currency;
    Action action;

    public Command(ref BankAccount account, Action action, Currency currency, float amount)
    {
        this.account = account;
        this.amount = amount;
        this.action = action;
        this.currency = currency;
    }

    public bool call()
    {
        switch (action)
        {
            case Action.deposit:
                return account.deposit(currency, amount);
            case Action.withdraw:
                return account.withdraw(currency, amount);
        }

        return false;
    }

    public void undo()
    {
        switch (action)
        {
            case Action.withdraw:
                account.deposit(currency, amount);
                break;
            case Action.deposit:
                account.withdraw(currency, amount);
                break;
        }
    }
}

public class CommandPattern : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        BankAccount ba = new BankAccount();
        CommandList commands = new CommandList
        {
            new Command(ref ba, Action.deposit, Currency.USD,  100),
            new Command(ref ba, Action.withdraw, Currency.USD, 200)
        };


        commands.call();

        Debug.Log(ba.balance);
        Debug.Log("\n");

        commands.undo();
        Debug.Log(ba.balance);

    }

    // Update is called once per frame
    void Update()
    {
    }
}
