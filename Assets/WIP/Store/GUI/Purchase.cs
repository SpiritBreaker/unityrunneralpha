﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Purchase : MonoBehaviour
{
	public GameObject Content;

    public void OnPointerDown()
    {
        UIDialogBox dialogBox = Instantiate(UIManager.Instance.YesNoDialog);
		dialogBox.PopUpDialog("Title", "Do you want purchase this item?", OK, Cancel);


        GameObject cont = Object.Instantiate(Content, Vector3.zero, Quaternion.identity);
		cont.GetComponent<RectTransform>().SetPositionAndRotation(Vector3.zero, Quaternion.identity);
		cont.transform.SetParent(dialogBox.Content.transform);
		dialogBox.Content.SetActive(true);
    }

    void OK()
    {
		Debug.Log("Start transaction");
    }

    void Cancel()
    {
		Debug.Log("Abort");
    }

}
