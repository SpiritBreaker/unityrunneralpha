﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OnlineStore;

public class TetsFillLayout : MonoBehaviour
{

    Transform DealsLayout;
    public GameObject prefab;
    public GameObject scrollRect;
    public Store store;


    public GameObject SimpleButton(Vector3 position, Vector2 size,
    UnityEngine.Events.UnityAction method)
    {
        GameObject button = new GameObject();

        button.AddComponent<RectTransform>();
        button.AddComponent<Image>();
        button.AddComponent<Button>();
        button.transform.position = position;
        //button.GetComponent<Button>().onClick.AddListener(method);
        return button;
    }

    public GameObject PrefabButton(Vector3 position, Vector2 size, Deal deal,
UnityEngine.Events.UnityAction method)
    {
        GameObject button = Instantiate(prefab, Vector3.zero, Quaternion.identity);
        button.transform.position = position;

        UIDeal uiDeal = button.GetComponent<UIDeal>();
        uiDeal.price.text = deal.price.ToString();
        uiDeal.amount.text = deal.amount.ToString();

        //button.GetComponent<Button>().onClick.AddListener(method);
        return button;
    }



    public GameObject CreateButton(Vector3 position, Vector2 size, Deal deal,
    UnityEngine.Events.UnityAction method)
    {
        if (prefab == null)
        {
            return SimpleButton(position, size, method);
        }
        else
        {
            return PrefabButton(position, size, deal, method);
        }
    }

    // Use this for initialization
    void Start()
    {
        DealsLayout = this.GetComponent<Transform>();
        for (int i = 0; i < store.deals.Count; i++)
        {
            
            GameObject button = CreateButton(Vector3.zero, Vector2.zero, store.deals[i], null);
            button.transform.SetParent(DealsLayout);
        }
        // 
        // for (int i = 0; i < 20; i++)
        // {

        // }



        scrollRect.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
