﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ControlAction 
{
    public abstract void TimeWhenActionStart();
}
