﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TapAction : ControlAction
{
    public abstract void Tap();
	
	public override void TimeWhenActionStart()
	{
		Debug.Log("ToDo get time");
	}
}
