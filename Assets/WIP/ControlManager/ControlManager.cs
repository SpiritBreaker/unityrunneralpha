﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;

public class ControlManager : Singleton<ControlManager>
{

    SwipeAction swipeAction;
    TapAction tapAction;


    public enum SwipeActionType
    {
        Jump, Run
    }

    public enum TapActionType
    {
        OneFinger, TwoFingers
    }

    [SerializeField, HideInInspector]
    public SwipeActionType swipeActionType;
    [SerializeField, HideInInspector]
    public TapActionType tapActionType;

    public float spacing = .1f;
    public float resolution = 1;
    // Use this for initialization
    void Start()
    {
        NewSwipeControlType();
        NewTapControlType();
    }

    public void NewSwipeControlType()
    {
        Type t = Type.GetType(swipeActionType.ToString());
        swipeAction = Activator.CreateInstance(t) as SwipeAction;
    }

    public void NewTapControlType()
    {
        Type t = Type.GetType(tapActionType.ToString());
        tapAction = Activator.CreateInstance(t) as TapAction;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            swipeAction.Do();
        }

        if (Input.GetMouseButtonDown(0))
        {
            tapAction.Tap();
        }
    }
}
