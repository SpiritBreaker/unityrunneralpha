﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraRender : MonoBehaviour
{
	public Material mat;

    // Use this for initialization
    void Start()
    {
        Camera.main.depthTextureMode = DepthTextureMode.Depth;
    }

    // Update is called once per frame
    // Called by the camera to apply the image effect
    // void OnRenderImage(RenderTexture source, RenderTexture destination)
    // {
    //     //mat is the material containing your shader
    //     Graphics.Blit(source, destination, mat);
    // }
}
