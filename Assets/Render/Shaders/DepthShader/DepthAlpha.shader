﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// http://flafla2.github.io/2016/10/01/raymarching.html - nice cool looking effect


Shader "Hidden/STS_save_orig_depth" 
{

	Properties 
	{
		_MainTex("Base(RGB)", 2D) = "white" {}
		[NoScaleOffset] _SkyCubemap ("-", Cube) = "" {}
	}

	SubShader 
	{
		Pass 
		{
			Tags { "Queue"="Transparent" "RenderType"="Transparent" "IgnoreProjector"="True" }

			// inside Pass
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha

 			CGPROGRAM
       		// use "vert" function as the vertex shader
       		#pragma vertex vert
       		// use "frag" function as the pixel (fragment) shader
       		#pragma fragment frag

       		// vertex shader inputs
       		struct appdata
       		{
       		    float4 vertex : POSITION; // vertex position
       		    float2 uv : TEXCOORD0; // texture coordinate
       		};

       		    // vertex shader outputs ("vertex to fragment")
       		struct v2f
       		{
       		    float2 uv : TEXCOORD0; // texture coordinate
       		    float4 vertex : SV_POSITION; // clip space position
       		};

       		// vertex shader
       		v2f vert (appdata v)
       		{
       		    v2f o;
       		    // transform position to clip space
       		    // (multiply with model*view*projection matrix)
       		    o.vertex = UnityObjectToClipPos(v.vertex);
       		    // just pass the texture coordinate
       		    o.uv = v.uv;
       		    return o;
       		}
	
       		// texture we will sample
       		sampler2D _MainTex;

       		// pixel shader; returns low precision ("fixed4" type)
       		// color ("SV_Target" semantic)
       		fixed4 frag (v2f i) : SV_Target
       		{
       		    // sample texture and return it
       		    fixed4 col = tex2D(_MainTex, i.uv);
       		    return col;
       		}
       		ENDCG
		}

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"		

			sampler2D _MainTex; 
			samplerCUBE _SkyCubemap;

			struct v2f
			{
			    float2 uv : TEXCOORD0;
			    float4 vertex : SV_POSITION;
			    float4 worldPos : TEXCOORD1;
			    float3 viewDir : TEXCOORD2;
			};

			v2f vert (appdata_full v)
			{
			    v2f o;
			    o.vertex = UnityObjectToClipPos(v.vertex);
			    o.uv = v.texcoord.xy;
			    o.worldPos = mul(unity_ObjectToWorld, v.vertex);
			    o.viewDir = normalize(UnityWorldSpaceViewDir(o.worldPos));
			    return o;
			}

			//Fragment Shader
			half4 frag (v2f i):COLOR {

				return tex2D(_MainTex, i.uv);

						// fixed4 colorBuffer = tex2D(_MainTex, i.uv);
						// float camDist = distance(i.worldPos, _WorldSpaceCameraPos);
						// half4 ray = half4(normalize(i.worldPos - _WorldSpaceCameraPos), 1.0);
						// half4 skyColor = half4(texCUBE(_SkyCubemap, ray));
						// half fog = exp2(-camDist/10 * camDist);
						// //return transparancy;
						// return lerp(skyColor, colorBuffer, fog);
			}
			ENDCG
		}


 	}
}
    	
