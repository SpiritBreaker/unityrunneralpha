﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// http://flafla2.github.io/2016/10/01/raymarching.html - nice cool looking effect


Shader "STS_save_orig_depth" {

	Properties {

		_MainTex("Base(RGB)", 2D) = "white" {}
		_Color("Color", Color) = (.34, .85, .92, 1)
		_Multiplay("Multiplay",  Range (0.0, 3.0)) = 1
		_Distance("Distance", Range (0, 5000)) = 400
		// _Tint ("Tint Color", Color) = (0.500000,0.500000,0.500000,0.500000)
		//[Gamma]  _Exposure ("Exposure", Range(0.000000,8.000000)) = 1.000000

		//[NoScaleOffset] _SkyCubemap ("-", Cube) = "" {}
	}

    SubShader 
	{
		Pass 
		{
		    CGPROGRAM
		    #pragma target 3.5
		    #pragma vertex vert
		    #pragma fragment frag
		    #include "UnityCG.cginc"		

		    sampler2D _MainTex; 
		    float4 _MainTex_ST;
		    float4 _Color;
		    float _Multiplay;
		    float4 _Tint;
		    float _Exposure;
		    float _Distance;
		    samplerCUBE _SkyCubemap;

		    struct v2f
		    {
    	    	float2 uv : TEXCOORD0;
    	    	float4 vertex : SV_POSITION;
    	    	float4 worldPos : TEXCOORD1;
    	    	float3 viewDir : TEXCOORD2;
		    };

		    v2f vert (appdata_full v)
		    {
    	    	v2f o;
    	    	o.vertex = UnityObjectToClipPos(v.vertex);
    	    	o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
    	    	o.worldPos = mul(unity_ObjectToWorld, v.vertex);
    	    	o.viewDir = normalize(UnityWorldSpaceViewDir(o.worldPos));
	
    	    	return o;
		    }

		    //Fragment Shader
		    half4 frag (v2f i):COLOR 
		    {	
		    	fixed4 colorBuffer = tex2D(_MainTex, i.uv) * _Color * _Multiplay;
		    	float camDist = distance(i.worldPos, _WorldSpaceCameraPos);
		    	half4 ray = half4(normalize(i.worldPos - _WorldSpaceCameraPos), 1.0);
		    	half4 skyColor = half4(texCUBE(_SkyCubemap, ray)) * _Tint * (_Exposure*4.0);

		    	half fog = exp2(-camDist/_Distance * camDist);
		    	//half4 transparancy =  skyColor * 1 - colorBuffer.a;
		    	return lerp(skyColor, colorBuffer, fog) ;
		    }
		    ENDCG
		}
	}
}