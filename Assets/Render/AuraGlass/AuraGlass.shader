﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Aura Glass" {

	Properties {

		_Color ("Color", Color) = (1,1,1,1)
		_SColor ("Secondary Color", Color) = (1,0,0,1)
		_Rim ("Rim", Float) = 1.0
		_Thr ("Threshold", Range(0.0, 1.0)) = 0.3
		
		//_Tint ("Tint Color", Color) = (0.500000,0.500000,0.500000,0.500000)
		//[Gamma]  _Exposure ("Exposure", Range(0.000000,8.000000)) = 1.000000
		//[NoScaleOffset] _SkyCubemap ("-", Cube) = "" {}
	}

	SubShader {
		Tags { "Queue" = "Transparent" }
		
		Pass {
			ZWrite Off 
			Blend SrcAlpha OneMinusSrcAlpha 
			Cull Back
			CGPROGRAM

			#pragma vertex vert 
			#pragma fragment frag
			#include "UnityCG.cginc" 

			uniform float4 _Color;
			uniform float _Rim;
			uniform float4 _SColor;
			uniform float _Thr;

			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal: NORMAL;
			};

			struct vertexOutput{
				float4 pos : SV_POSITION;
				float3 normal : TEXCOORD0;
				float3 viewDir: TEXCOORD1;
			};

			vertexOutput vert(vertexInput input){ 
				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.normal = normalize(mul(float4(input.normal,0.0), unity_WorldToObject).xyz);
				output.viewDir = normalize(_WorldSpaceCameraPos - mul(unity_ObjectToWorld, input.vertex).xyz);

				return output;
			}

			float4 frag(vertexOutput input) : COLOR { 

				float3 viewDir = normalize(input.viewDir);
				float3 norm = normalize(input.normal);
				float newOpacity = min(1.0, _Color.a / abs((pow(dot(viewDir, norm), _Rim)) - (0.3 - _Color.a )));
				if (newOpacity > _Thr){ 
					float3 variationColor = _Color.xyz + newOpacity * _SColor.xyz;
					return float4(variationColor, newOpacity);

				}else{ 
					float3 variationColor = _Color.xyz;
					return float4(variationColor, newOpacity);
				}
			}
						
			ENDCG
		}
		
		Pass {
			ZWrite Off 
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Front
			CGPROGRAM

			#pragma vertex vert 
			#pragma fragment frag
			#include "UnityCG.cginc" 

			uniform float4 _Color;
			uniform float _Rim;
			uniform float4 _SColor;
			uniform float _Thr;


    	    float4 _Tint;
            float _Exposure;
			samplerCUBE _SkyCubemap;

			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal: NORMAL;
			};

			struct vertexOutput{
				float4 pos : SV_POSITION;
				float3 normal : TEXCOORD0;
				float3 viewDir: TEXCOORD1;
				float4 worldPos : TEXCOORD2;
			};

			vertexOutput vert(vertexInput input){ 

				vertexOutput output;
				output.pos = UnityObjectToClipPos(input.vertex);
				output.normal = normalize(mul(float4(input.normal,0.0), unity_WorldToObject).xyz);
				output.viewDir = normalize(_WorldSpaceCameraPos - mul(unity_ObjectToWorld, input.vertex).xyz);
				output.worldPos = mul(unity_ObjectToWorld, input.vertex);
				return output;

			}

			float4 frag(vertexOutput input) : COLOR{

				float3 viewDir = normalize(input.viewDir);
				float3 norm = normalize(input.normal);
				fixed4 colorBuffer = float4(_SColor.rgb, _SColor.a - abs(dot(viewDir, norm)));
				
				float camDist = distance(input.worldPos, _WorldSpaceCameraPos);

			    half4 ray = half4(normalize(input.worldPos - _WorldSpaceCameraPos), 1.0);
			    half4 skyColor = half4(texCUBE(_SkyCubemap, ray)) * _Tint * (_Exposure*4.0);
			    half fog = exp2(-camDist/400 * camDist);
			    return lerp(skyColor, colorBuffer, fog);

				//return colorBuffer;
			}
	
			ENDCG
		}
	}
}