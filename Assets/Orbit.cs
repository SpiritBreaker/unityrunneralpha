﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour {

	public Transform obj;
	Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = this.gameObject.GetComponent<Rigidbody>();
		rb.AddTorque(Vector3.right * 5f, ForceMode.Impulse);

	}
	
	// Update is called once per frame
	void Update () {
				rb.MovePosition(obj.transform.position);
	}
}
