﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{

    public float currentAngleSpeed;
    public float currentAngle;
    public float currentAngleRotLocal;
    public float rotateSpeed;
    private Vector2 startPos;
    public float minSwipeDistX;
    private List<float> speedHistory;
	public float maxRotSpeed;

	public Transform sphere;


    public Vector3 rotation;
    // Use this for initialization
    void Start()
    {
		speedHistory = new List<float>();
    }

    // Update is called once per frame
    void Update()
    {
        this.UpdateInput();
        this.currentAngleSpeed = Mathf.Lerp(this.currentAngleSpeed, 0.0f, 1f * Time.deltaTime);
        this.currentAngle += this.currentAngleSpeed * Time.deltaTime;
		//sphere.transform.Translate(-Vector3.up * (Mathf.Abs(this.currentAngleSpeed)/50f) * Time.deltaTime);
        this.currentAngleRotLocal = Mathf.Lerp(this.currentAngleRotLocal, (float)((int)((double)this.currentAngle / 10.0) * 10), 20f * Time.deltaTime);
        this.transform.localRotation = Quaternion.Euler(new Vector3(0.0f, this.currentAngle, 0.0f));
    }


    private void UpdateInput()
    {
        float num1 = Mathf.Clamp((new Vector3(Input.mousePosition.x, 0.0f, 0.0f) - new Vector3(this.startPos.x, 0.0f, 0.0f)).magnitude, 0.0f, this.maxRotSpeed);
        float num2 = num1 / (float)Screen.width;
        float num3 = -Mathf.Sign(Input.mousePosition.x - this.startPos.x) * num2 * this.rotateSpeed;

        if (Input.GetMouseButtonDown(0))
        {
            this.currentAngleSpeed = 0.0f;
            this.startPos = (Vector2)Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            this.speedHistory.Clear();
            this.currentAngleSpeed = 0.0f;
            if ((double)num2 > (double)this.minSwipeDistX)
                this.speedHistory.Add(num3);
            else
                this.speedHistory.Add(0.0f);
            if (this.speedHistory.Count > 4)
                this.speedHistory.RemoveAt(0);
            this.currentAngle += num3;
			//sphere.position = new Vector3(sphere.position.x, sphere.position.y - Mathf.Abs(num3/50f), sphere.position.z);
            this.startPos = (Vector2)Input.mousePosition;
        }
        else
        {
            if (!Input.GetMouseButtonUp(0) || (double)num1 <= (double)this.minSwipeDistX)
                return;
            float num4 = 0.0f;
            for (int index = 0; index < this.speedHistory.Count; ++index)
                num4 += this.speedHistory[index];
            this.currentAngleSpeed = 36f * num4;
            this.startPos = (Vector2)Input.mousePosition;
        }
    }

}
