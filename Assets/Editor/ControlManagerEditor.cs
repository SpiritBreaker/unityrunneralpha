﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ControlManager))]
public class ControlManagerEditor : Editor
{

    bool switchType;
    ControlManager.SwipeActionType swipeType;
    ControlManager.TapActionType tapType;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUI.BeginChangeCheck();

        swipeType = (ControlManager.SwipeActionType)EditorGUILayout.EnumPopup("Change swipe type", ControlManager.Instance.swipeActionType);
        if (swipeType != ControlManager.Instance.swipeActionType)
        {
            ControlManager.Instance.swipeActionType = swipeType;
            ControlManager.Instance.NewSwipeControlType();
        }

        tapType = (ControlManager.TapActionType)EditorGUILayout.EnumPopup("Change tap type", ControlManager.Instance.tapActionType);
        if (tapType != ControlManager.Instance.tapActionType)
        {
            ControlManager.Instance.tapActionType = tapType;
            ControlManager.Instance.NewTapControlType();
        }


    }
}
