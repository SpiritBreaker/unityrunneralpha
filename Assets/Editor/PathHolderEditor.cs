﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(PathHolder))]
public class PathHolderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        PathHolder myScript = (PathHolder)target;
        if(GUILayout.Button("Set Path Points"))
        {
            myScript.SetPathPoints();
        }
    }
}

#endif