﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(Account))]
public class AccountrEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        Account myScript = (Account)target;
        if(GUILayout.Button("Erase Account"))
        {
            if(File.Exists(Application.persistentDataPath + "/player.sav"))
            {   
                File.Delete(Application.persistentDataPath + "/player.sav");
            }
        }
    }
}

#endif