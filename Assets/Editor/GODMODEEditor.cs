﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(GODMODE))]
public class GODMODEEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    
        if(GUILayout.Button("GameOver"))
        {
            Debug.Log("button pressed");
            UIManager.Instance.showAd = true;
			Game.Instance.gamePause();
        }
    }
}

#endif