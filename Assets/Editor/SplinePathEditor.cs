﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;




[CustomEditor(typeof(SplinePathCreator))]
public class SplinePathEditor : Editor
{
    int selectedIndex = -1;

    private Transform handleTransform;
    private Quaternion handleRotation;

    SplinePathCreator creator;
    SplinePath Path
    {
        get
        {
            return creator.path;
        }
    }

    const float segmentSelectDistanceThreshold = 100.1f;
    int selectedSegmentIndex = -1;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUI.BeginChangeCheck();
        if (GUILayout.Button("Create new"))
        {
            Undo.RecordObject(creator, "Create new");
            creator.CreatePath();
        }

        bool isClosed = GUILayout.Toggle(Path.IsClosed, "Closed");

        if (isClosed != Path.IsClosed)
        {
            Undo.RecordObject(creator, "Toogle closed");
            Path.IsClosed = isClosed;
        }

        bool autoSetControlPoints = GUILayout.Toggle(Path.AutoSetControlPoints, "Auto Set Control Points");
        if (autoSetControlPoints != Path.AutoSetControlPoints)
        {
            Undo.RecordObject(creator, "Toogle auto set controls");
            Path.AutoSetControlPoints = autoSetControlPoints;
        }
        if (EditorGUI.EndChangeCheck())
        {
            SceneView.RepaintAll();
        }
    }


    void OnSceneGUI()
    {
        //Input();

        handleTransform = creator.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ?
            handleTransform.rotation : Quaternion.identity;

        ExtendedDraw();
    }


    Vector3 MouseHitPosition()
    {
        var p = new Plane(creator.transform.TransformDirection(Vector3.up), creator.transform.position);

        // build a ray type from the current mouse position
        var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

        // stores the hit location
        var hit = new Vector3();

        // stores the distance to the hit location
        float dist;

        // cast a ray to determine what location it intersects with the plane
        if (p.Raycast(ray, out dist))
        {
            // the ray hits the plane so we calculate the hit location in world space
            hit = ray.origin + (ray.direction.normalized * dist);
        }

        // convert the hit location from world space to local space
        var value = creator.transform.InverseTransformPoint(hit);

        return hit;
    }

    void ExtendedInput()
    {

    }

    void LegacyInput()
    {
        Event guiEvent = Event.current;
        Vector3 mousePos = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition).origin;

        if (guiEvent.type == EventType.MouseDown && guiEvent.button == 0 && guiEvent.shift)
        {
            if (selectedSegmentIndex != -1 && guiEvent.control)
            {
                Undo.RecordObject(creator, "Split Segment");
                Vector3[] points = Path.GetPointInSegment(selectedSegmentIndex);
                float dst = HandleUtility.DistancePointBezier(mousePos, points[0], points[3], points[1], points[2]);
                Vector3 bezierPos = Bezier.EvaluateCubic(points[0], points[3], points[1], points[2], dst);
                Path.SplitSegment(bezierPos, selectedSegmentIndex);
            }

            else if (!Path.IsClosed)
            {
                Undo.RecordObject(creator, "Add Segment");
                Path.AddSegment(MouseHitPosition());
            }
        }


        if (guiEvent.type == EventType.MouseDown && guiEvent.button == 1)
        {
            float minDistToAnchor = creator.anchorDiameter * 0.5f;
            int closestAnchorIndex = -1;

            for (int i = 0; i < Path.NumPoints; i += 3)
            {
                float dst = Vector3.Distance(mousePos, Path[i]);
                if (dst < minDistToAnchor)
                {
                    minDistToAnchor = dst;
                    closestAnchorIndex = i;
                }
            }

            if (closestAnchorIndex != -1)
            {
                Undo.RecordObject(creator, "Delete segment");
                Path.DeleteSegment(closestAnchorIndex);
            }
        }

        if (guiEvent.type == EventType.MouseMove)
        {


            float minDstToSegment = segmentSelectDistanceThreshold;
            int newSelectedSegmentIndex = -1;

            for (int i = 0; i < Path.NumSegments; i++)
            {
                Vector3[] points = Path.GetPointInSegment(i);
                float dst = HandleUtility.DistancePointBezier(mousePos, points[0], points[3], points[1], points[2]);

                if (dst < minDstToSegment)
                {
                    minDstToSegment = dst;
                    newSelectedSegmentIndex = i;
                }
            }

            if (newSelectedSegmentIndex != selectedSegmentIndex)
            {
                selectedSegmentIndex = newSelectedSegmentIndex;
                HandleUtility.Repaint();
            }
        }
    }

    void ExtendedDraw()
    {
        for (int i = 0; i < Path.NumSegments; i++)
        {
            Vector3[] points = Path.GetPointInSegment(i);

            for (int p = 0; p < points.Length; p++)
            {
                points[p] = handleTransform.TransformPoint(points[p]);
            }

            if (creator.displayControlPoints)
            {
                Handles.color = Color.black;
                Handles.DrawLine(points[1], points[0]);
                Handles.DrawLine(points[2], points[3]);
            }
            Color segmentCol = (i == selectedSegmentIndex && Event.current.shift) ? creator.selectedSegmentCol : creator.segmentCol;
            Handles.DrawBezier(points[0], points[3], points[1], points[2], segmentCol, null, 2);
        }

        for (int i = 0; i < Path.NumPoints; i++)
        {
            if (i % 3 == 0 || creator.displayControlPoints)
            {

                Vector3 point = handleTransform.TransformPoint(Path[i]);

                Handles.color = (i % 3 == 0) ? creator.anchorColor : creator.controlCol;
                float handleSize = (i % 3 == 0) ? creator.anchorDiameter : creator.controlDiamter;

                if (Handles.Button(point, handleRotation, handleSize, 0.06f, Handles.SphereHandleCap))
                {
                    selectedIndex = i;
                    Repaint();
                }

                if (selectedIndex == i)
                {
                    Vector3 test = Handles.PositionHandle(point, handleRotation);
                    if (point !=  test)
                    {
                        Undo.RecordObject(creator, "MovePoint");
                        Path.MovePoint(i,  handleTransform.InverseTransformPoint(test));
                    }
                }
            }
        }
    }


    void OnEnable()
    {
        creator = (SplinePathCreator)target;
        if (creator.path == null)
        {
            creator.CreatePath();
        }
    }
}
