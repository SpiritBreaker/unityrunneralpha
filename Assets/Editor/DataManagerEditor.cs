﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataModel;

#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(DataManager))]
public class DataManagerEditor : Editor
{

	public string GetFormatedString()
	{
		string dataSting = "";
		foreach (KeyValuePair<string, List<SerializableObject>> item in DataManager.Instance.Storage())
		{
			dataSting += item.Key + " " + item.Value.Count + "\n";
		}
		return dataSting;
	}
	
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GUILayout.TextField(GetFormatedString());

    }

}

#endif