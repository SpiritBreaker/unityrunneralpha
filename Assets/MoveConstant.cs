﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveConstant : MonoBehaviour {

	
	public Transform other;
	public float speed = 10f;
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
				 transform.position = Vector3.MoveTowards(transform.position, other.position, Time.deltaTime * speed);
	}
}
